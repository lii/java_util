package j.util.res;

import java.io.BufferedInputStream;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


class StringRes extends Res<String>
{
	
}

class ResObj
{
	public static String HELLO_MSG = "Default hello message";
	public static int MAX_WIDTH = 17;
}

public class ResourceHandler
{
	
	private Map<String, Res<?>> resMap = new HashMap<String, Res<?>>(); 
	
	public <T> void load( Class<?> resClass ) throws IllegalArgumentException, IllegalAccessException
	{
		resMap = loadResourceMap( resClass );
		
		for ( Field f : resClass.getFields() ) {
			@SuppressWarnings( "rawtypes" ) Res res = new Res();
			res.setKey( f.getName() );
			res.setFamily( resClass.getName() );
			readValue( res );
			
			f.set( null, res.getValue() );
			
			addRes( res );
		}
	}
	
	private void addRes( Res<?> res ) {
		resMap.put( res.getKey(), res );
	}
	
    public Res<?> getResource( String key ) {
		return resMap.get( key );
	}
	
    
    public HashMap<String, Res<?>> loadResourceMap( Class<?> c )
    {
    	return null;
    }
    
	public BufferedInputStream getResourceFile()
	{
		return null;
	}
	
	
	public Object readValue( Res<?> name ) {
		return null;
	}
}











class Res<T>
{
	private T value;
	private File file;
	private String key;
	private String family;
	
	public T getValue() {
		return value;
	}

	public File getFile() {
    	return file;
    }

	public void setFile( File file ) {
    	this.file = file;
    }

	public String getKey() {
    	return key;
    }

	public void setKey( String key ) {
    	this.key = key;
    }

	public void setValue( T value ) {
    	this.value = value;
    }

	public String getFamily() {
    	return family;
    }

	public void setFamily( String family ) {
    	this.family = family;
    }

	@Override
    public boolean equals( Object obj ) {
		if (!(obj instanceof Res<?>)) return false;
		Res<?> other = (Res<?>) obj;
		
	    return getKey().equals( other.getKey() )
	    	&& getFamily().equals( other.getFamily() );
    }

	@Override
    public int hashCode() {
	    return getKey().hashCode();
    }
	
	
	
}

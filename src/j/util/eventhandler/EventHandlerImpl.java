package j.util.eventhandler;

import j.util.functional.Fun1;
import j.util.lists.Lists;
import j.util.lists.Trees;
import j.util.tree.Tree;
import j.util.tree.TreeImpl;
import j.util.util.Asserts;
import j.util.util.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Iterables;


public class EventHandlerImpl extends AbstractEventHandler implements EventHandler
{
	private Tree<Group> receiverTree = new TreeImpl<Group>( GROUP_COMP );

    @SuppressWarnings("rawtypes")
	private Map<Pair<GroupName, Class>, List<Receiver>> receiverCache;

    @SuppressWarnings( "rawtypes" )
    private Map<GroupName, List<Receiver>> simpleReceiverCache;


	private boolean throwOnNoRec = true;
	private Cashing caching = Cashing.PREBUILD;

	private static final Object NULL_OBJECT = new Object() {};

	public void post( GroupName name, Object e )
    {
    	switch ( caching ) {
    		case OFF: postNoCache( name, e ); break;
    		case LAZY: postLazyCache( name, e ); break;
    		case PREBUILD: postPrebuiltCache( name, e ); break;
    		case SIMPLE_PREBUILD: postSimplePrebuiltCache( name, e ); break;

    	}
    }

    public void postEmpty( GroupName name )
    {
    	post( name, NULL_OBJECT );
    }


    // Looks up receivers from a prebuild cache
    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public void postPrebuiltCache( GroupName name, Object e )
	{
		List<Receiver> rs = Lists.nullToEmpyList( receiverCache.get( Pair.make( name, e.getClass() ) ) );

    	// This could be legal, but a warning is good
    	checkForReceivers( name, e, rs );

    	if ( e == NULL_OBJECT ) {
    		e = null;
    	}

    	for ( Receiver r : rs ) {
    		r.receive( e );
    	}
	}

	private void checkForReceivers( GroupName name, Object e, @SuppressWarnings( "rawtypes" ) List<Receiver> rs )
			throws IllegalStateException {
		if ( throwOnNoRec && rs.isEmpty() ) {
    		throw new IllegalStateException( "Event " + e.getClass() +
    				" has no receivers in group " + name );
    	}
	}

    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public void postSimplePrebuiltCache( GroupName name, Object e )
	{
    	List<Receiver> rs = Lists.nullToEmpyList( simpleReceiverCache.get( name ) );

    	checkForReceivers( name, e, rs );

    	if ( e == NULL_OBJECT ) {
    		e = null;
    	}

    	for ( Receiver r : rs ) {
    		r.receive( e );
    	}
	}

    // Finds receivers from the tree each time
    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public void postNoCache( GroupName name, Object e )
	{
	    Tree<Group> subtree = receiverTree.findSubtree( name );

//    	if ( e == NULL_OBJECT ) {
//    		e = null;
//    	}

	    Iterable<Group> groups = Iterables.concat( subtree, subtree.getParents() );

		for ( Group group : groups ) {
			for ( Receiver rec : group.getReceivers( e.getClass() ) ) {
				rec.receive( e );
			}
	    }
	}


    // Builds cache when a event is posted
    @SuppressWarnings( { "rawtypes" } )
    private static Map<GroupName, List<Receiver>> buildSimpleCache( Tree<Group> receivers )
    {
    	Map<GroupName, List<Receiver>> result = new HashMap<GroupName, List<Receiver>>();

		for ( Tree<Group> subtree : receivers.subtreeIterable() ) {

			ArrayList<Receiver> groupReceivers = new ArrayList<Receiver>();

			for ( Group group : Iterables.concat( subtree, subtree.getParents() ) ) {
				groupReceivers.addAll( group.getReceivers() );
		    }

			if ( !groupReceivers.isEmpty() ) {
				groupReceivers.trimToSize();
				result.put( subtree.getData().getName(), groupReceivers );
			}
		}

    	return result;
    }


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    private static Map<Pair<GroupName, Class>, List<Receiver>>
    	buildCache( Tree<Group> receivers )
    {
    	Set<Class<?>> eventTypes = new HashSet<Class<?>>();
    	Map<Pair<GroupName, Class>, List<Receiver>> result = new HashMap<Pair<GroupName, Class>, List<Receiver>>();

    	for ( Group g : receivers ) {
    		eventTypes.addAll( g.getEventTypes() );
    	}

    	for ( Class eventClass : eventTypes ) {
			for ( Tree<Group> subtree : receivers.subtreeIterable() ) {

				ArrayList<Receiver> rs = new ArrayList<Receiver>();

				for ( Group group : Iterables.concat( subtree, subtree.getParents() ) ) {
					rs.addAll( group.getReceivers( eventClass ) );
			    }

				if ( !rs.isEmpty() ) {
					rs.trimToSize();
					result.put( Pair.make( subtree.getData().getName(), eventClass ), rs );
				}
			}
    	}

    	return result;
    }

	@SafeVarargs
	public static Tree<GroupName> g( GroupName name,  Tree<GroupName>... ts ) {
		return new TreeImpl<GroupName>( name, (TreeImpl<GroupName>[]) ts);
	}

	public void addReceiversFromClass( Class<?> cls ) {

		Tree<GroupName> nameTree = Trees.buildFromClass( cls );


		receiverTree = nameTree.transp( new Fun1<GroupName, Group>() {
			public Group run( GroupName arg ) {
				return new Group( arg );
			}
		});
		receiverTree.setComparator( GROUP_COMP );

		rebuildCache();
	}

	@SuppressWarnings( "unchecked" )
    public void addGroups( Object o ) {
		Tree<GroupName> nameTree = (Tree<GroupName>) o;

		for ( Tree<GroupName> tree : nameTree.subtreeIterable() ) {
			receiverTree.findSubtree( tree.getParent() )
				.concat( new TreeImpl<Group>( new Group( tree.getData() ) ) );

		}

		rebuildCache();
	}

	@Override
	public <E> void addReceiver( GroupName g, Class<E> cls, Receiver<?> r )
	{
		Group group = receiverTree.findData( g );
		Asserts.notNull( group, "Trying to add receiver with unknown name" );
		group.add( cls, r );

		rebuildCache();
	}

	@Override
    public void addReceiver( GroupName g, NoArgReceiver r )
	{
    	addReceiver( g, NULL_OBJECT.getClass(), r );
	}

    @Override
    public void addGroup( GroupName name ) {
		receiverTree.setData( new Group( name ) );
		rebuildCache();
	}

    private void rebuildCache() {
    	switch ( caching ) {
    		case PREBUILD:
    			receiverCache = buildCache( receiverTree ); break;
    		case SIMPLE_PREBUILD:
    			simpleReceiverCache = buildSimpleCache( receiverTree ); break;
    		default: break;
    	}
	}

    @Override
    public void addGroup( GroupName name, GroupName parent )
	{
		Asserts.arg( !receiverTree.contains( name ),
				"Eventhandler already contains group with this name"  );

		receiverTree.findSubtree( parent ).add( new Group( name ) );
		rebuildCache();
	}


    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public void postLazyCache( GroupName name, Object e )
	{
    	Pair<GroupName, Class> key = new Pair<GroupName, Class>( name, e.getClass() );

    	// TODO:
//    	if ( e == NULL_OBJECT ) {
//    		e = null;
//    	}
    	List<Receiver> rs = receiverCache.get( key );

    	if ( rs == null ) {
    		ArrayList<Receiver> newRs = new ArrayList<Receiver>();
    		Tree<Group> subtree = receiverTree.findSubtree( name );

    		for ( Group group : Iterables.concat( subtree, subtree.getParents() ) ) {
    			for ( Receiver r : group.getReceivers( e.getClass() ) ) {
    				newRs.add( r );
    			}
    	    }

    	    newRs.trimToSize();
    	    receiverCache.put( key, newRs );
    	    rs = newRs;
    	}

    	for ( Receiver r : rs ) {
    		r.receive( e );
    	}
	}


    // Test how it feels to have a separate settings object
    // Feels kinda good.
    @Override
    public Settings getSettings() {
    	return new SettingsImpl();
    }

    public class SettingsImpl implements Settings
	{
	    public Cashing getCashingStrat() {
	    	return caching;
	    }
		public Settings setCasheStrategy( Cashing c ) {
	    	caching = c;
	    	return this;
	    }
		public Settings setThrowOnNoReceiver( boolean b ) {
			throwOnNoRec = b;
			return this;
		}
		public boolean isThrowOnNoRec() {
			return throwOnNoRec;
		}
    }

    public void settings( Setting... setts ) {
    	for ( Setting s : setts ) {
    		switch ( s ) {
				case CACHE_OFF: caching = Cashing.OFF; break;
				case LAZY_CASHE:
					caching = Cashing.LAZY;
					rebuildCache();
					break;
				case PREBUILD_CACHE: caching = Cashing.SIMPLE_PREBUILD; break;
				case NOT_THROW_ON_NO_REC: throwOnNoRec = false; break;
				case THROW_ON_NO_REC: throwOnNoRec = true; break;
    		}
    	}
    }

	public final static Comparator<Object> GROUP_COMP = new Comparator<Object>() {
		@Override public int compare( Object g1, Object g2 ) {
			// Compare two groups by comparing their name
			if ( g1 instanceof Group ) g1 = ((Group)g1).getName();
			if ( g2 instanceof Group ) g2 = ((Group)g2).getName();

			return (g1 instanceof GroupName) && (g1 instanceof GroupName) ?
				((GroupName)g1).getName().compareTo( ((GroupName)g2).getName() )
				: -1;
		}
	};


}




package j.util.eventhandler;

import java.lang.reflect.Field;

public class EventHandlers
{
	public static void initGroupNames( Class<?> cls )
	{
		try {
			initGroupNamesImpl( cls );
		} catch ( SecurityException e ) {
			throw new RuntimeException( e );
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException( e );
		} catch ( IllegalAccessException e ) {
			throw new RuntimeException( e );
		}
	}

	private static void initGroupNamesImpl( Class<?> cls ) throws SecurityException, IllegalArgumentException, IllegalAccessException
	{
		for ( Field f : cls.getFields() ) {
			if ( ! (f.getType().equals( GroupName.class ) ) ) continue;
			f.set( null, new GroupName( f.getName() ) );
		}
	}
}

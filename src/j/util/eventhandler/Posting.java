package j.util.eventhandler;

public class Posting
{
	private Object event;
	private GroupName group;

	public Posting( GroupName group, Object event )
	{
		this.event = event;
		this.group = group;
	}

	public Posting( GroupName group )
	{
		this( group, null );
	}

	public Object getEvent() {
		return event;
	}

	public void setEvent( Object event ) {
		this.event = event;
	}

	public GroupName getGroup() {
		return group;
	}

	public void setGroup( GroupName group ) {
		this.group = group;
	}

}

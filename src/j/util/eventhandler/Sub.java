package j.util.eventhandler;

/**
 * Event subscription. Used for aggregating the info that is needed to register
 * as the receiver of events.
 *
 * Passed to an EventHandler to create a subscription.
 */
public class Sub {

	// The Group the receivers listens to
	private final GroupName group;

	// The class of the event object passed to rec.
	// As a hack: null if rec is a No Arg Receiver
	private final Class<?> cls;

	// The receiver callback, called when an event is sent the correct group
	private final Receiver<?> rec;

	public <T> Sub(GroupName group, Class<T> cls, Receiver<? extends T> rec) {
		this.group = group;
		this.cls = cls;
		this.rec = rec;
	}

	public Sub(GroupName group, NoArgReceiver rec) {
		this.group = group;
		this.cls = null;
		this.rec = rec;
	}

	public GroupName getGroup() {
		return group;
	}

	public Class<?> getCls() {
		return cls;
	}

	public Receiver<?> getRec() {
		return rec;
	}
}

package j.util.eventhandler;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;




//@interface Node {
//	Root parent() default @Root(); 
//}
//
//@interface Root {
//}



@Retention(RetentionPolicy.RUNTIME)
public @interface Parent {
	public String value() default "";
}

package j.util.eventhandler;

import j.util.util.Util;

public class GroupName implements Comparable<GroupName>
{
	private final String name;

	public GroupName( String name ) {
	    this.name = name;
    }

	public String getName() {
    	return name;
    }

	public int compareTo( GroupName other ) {
	    return name.compareTo( other.name );
    }

	@Override
    public String toString() {
	    return Util.simpleToString( this, name );
    }
}

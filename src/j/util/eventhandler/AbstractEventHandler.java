package j.util.eventhandler;

import java.util.List;

public abstract class AbstractEventHandler implements EventHandler {

	@Override public void post( Posting p ) {
		post( p.getGroup(), p.getEvent() );
	}

	public void post( GroupName name, List<?> events ) {
		for ( Object e : events ) {
			post( name, e );
		}
	}

	public void post( Iterable<Posting> list ) {
		for ( Posting p : list ) {
			post( p );
		}
	}

	@Override public void addReceivers( List<Sub> subs ) {
		for ( Sub rec : subs ) {
			addReceiver( rec );
		}
	}

	@Override public void addReceiver( Sub rec ) {
		if ( rec.getCls() == null ) {
			addReceiver( rec.getGroup(), (NoArgReceiver) rec.getRec() );
		} else {
			addReceiver( rec.getGroup(), rec.getCls(), rec.getRec() );
		}
	}

}

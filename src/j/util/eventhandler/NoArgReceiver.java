package j.util.eventhandler;


/**
 * Kind of Receivers that doent expect a event object, for notifications that
 * does not require tranfer of any data.
 *
 * Is called linke a normal receiver with a null argument. Its an error to call
 * it with anything other than null.
 */
public abstract class NoArgReceiver implements Receiver<Void>
{
	@Override
	public void receive( Void event )
	{
		receive();
    }

	public abstract void receive();
}

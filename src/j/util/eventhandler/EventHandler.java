package j.util.eventhandler;

import java.util.List;

public interface EventHandler
{
	public void post( GroupName name, Object e );
	public void post( GroupName name, List<?> events );
	public void post( Posting p );
	public void post( Iterable<Posting> p );
	public void postEmpty( GroupName name);

	public void addGroup( GroupName name );
	public void addGroup( GroupName group, GroupName parent );
	public void addGroups( Object o );

	public <E> void addReceiver( GroupName g, Class<E> cls, Receiver<?> r );
	public void addReceiver( GroupName g, NoArgReceiver r );
	public void addReceiver( Sub rec );
	public void addReceivers( List<Sub> subs );
	public void addReceiversFromClass( Class<?> cls );

	public Settings getSettings();
	public void settings( Setting... setts );


	public enum Setting {
		THROW_ON_NO_REC,
		NOT_THROW_ON_NO_REC,

		CACHE_OFF,
		LAZY_CASHE,
		PREBUILD_CACHE,
	}

	public enum Cashing {
		OFF,
		LAZY,
		PREBUILD,
		SIMPLE_PREBUILD,
	}

	// Not implemented
	public enum Breadth {
		PARENTS,
		SUBTREE,
	}

	public interface Settings {
		public Settings setThrowOnNoReceiver( boolean b );
		public boolean isThrowOnNoRec();
	    public Cashing getCashingStrat();
		public Settings setCasheStrategy( Cashing c );
	}
}

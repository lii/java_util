package j.util.eventhandler;

import j.util.lists.Lists;
import j.util.util.Asserts;
import j.util.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class Group
{
	private GroupName name;
	private Class<?> cls;
	@SuppressWarnings( "rawtypes" )
	private List<Receiver> receivers = new ArrayList<Receiver>( 0 );

	public Group( String name ) {
	    this( new GroupName( name ) );
    }

	public Group( GroupName name ) {
	    this.name = name;
    }

	public GroupName getName() {
    	return name;
    }

	public Collection<? extends Class<?>> getEventTypes() {
		List<? extends Class<?>> list = Arrays.asList( cls );
		return list;
	}

    @SuppressWarnings( "rawtypes" )
    public <E> List<Receiver> getReceivers( Class<E> cls ) {
		return Lists.nullToEmpyList( receivers );
    }

    @SuppressWarnings( "rawtypes" )
    public <E> List<Receiver> getReceivers() {
		return Lists.nullToEmpyList( receivers );
    }

//	public List<NoArgReceiver> getNoArgReceivers() {
//		return noArgReceivers;
//	}

    @SuppressWarnings( { "rawtypes", "unchecked" } )
    public <E> void add( Class<?> cls, Receiver<E> res ) {
    	Asserts.arg( !receivers.contains( res ), "Trying to add same receiver twise" );
    	Asserts.notNull( cls );

    	if ( this.cls == null ) {
    		this.cls = cls;
    	} else {
    		Asserts.arg( this.cls.isAssignableFrom( cls ), "Trying to add receiver of wrong type" );
    	}

    	if ( receivers.isEmpty() ) {
			receivers = new ArrayList<Receiver>();
		}

		receivers.add( (Receiver<Object>) res );
    }

//    public <E> void add( NoArgReceiver res ) {
//    	noArgReceivers.add( res );
//    }

	@Override
    public String toString() {
	    return Util.simpleToString( this, name );
    }




}


class Group_ManyTypes
{
	private GroupName name;

    @SuppressWarnings( "rawtypes" )
    private Map<Class<?>, List<Receiver>> receivers =
		new HashMap<Class<?>, List<Receiver>>();

//	private List<NoArgReceiver> noArgReceivers = new ArrayList<NoArgReceiver>();


//	public Group( String name ) {
//	    this( new GroupName( name ) );
//    }
//
//	public Group( GroupName name ) {
//	    this.name = name;
//    }

	public GroupName getName() {
    	return name;
    }

	public Set<Class<?>> getEventTypes() {
		return receivers.keySet();
	}

    @SuppressWarnings( "rawtypes" )
    public <E> List<Receiver> getReceivers( Class<E> cls ) {

    	// Code for accepting events that is subclasses.
    	// This turned out to be tricky. Why?
//		List<EventReceiver> result = null;
//
//		for ( Class<?> c : receivers.keySet() ) {
//			if ( c.isAssignableFrom( cls ) ) {
//				if ( result == null ) result = new LinkedList<EventReceiver>();
//				result.addAll( receivers.get( c ) );
//			}
//		}
//
//		return Lists.nullToEmpyList( result );
		return Lists.nullToEmpyList( receivers.get( cls ) );
    }

//	public List<NoArgReceiver> getNoArgReceivers() {
//		return noArgReceivers;
//	}

    @SuppressWarnings( { "rawtypes", "unchecked" } )
    public <E> void add( Class<?> cls, Receiver<E> res ) {
		List<Receiver> rs = receivers.get( cls );

		if ( rs == null ) {
			rs = new LinkedList<Receiver>();
			receivers.put( cls, rs );
		}

		rs.add( (Receiver<Object>) res );
    }

//    public <E> void add( NoArgReceiver res ) {
//    	noArgReceivers.add( res );
//    }

	@Override
    public String toString() {
	    return Util.simpleToString( this, name );
    }




}

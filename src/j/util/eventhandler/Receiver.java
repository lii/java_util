package j.util.eventhandler;

public interface Receiver<E>
{
	public void receive( E event );
}

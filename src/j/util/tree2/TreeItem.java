package j.util.tree2;

import j.util.functional.Fun1;
import j.util.functional.util.Funcs;
import j.util.lists.ArrayMap;
import j.util.tree.EntryIterator;
import j.util.tree.TreeNode;
import j.util.util.Util;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * A base class that can be extended by classes, so that they can be used as trees.
 *
 * A subclass needs only add type info and the domain specific data fields.
 *
 * K is the key to find a child. S is a self type.
 */
public class TreeItem<K, S extends TreeItem<K, S>> implements TreeNode<S>, Iterable<S>
{
	protected S parent;

	protected Map<K, S> children = Collections.emptyMap();
	protected Fun1<S, K> keyFun = Funcs.id();

	public TreeItem( Iterable<S> children ) {
		for ( S c : children ) {
			addChild( c );
		}
	}


	public S getParent() {
		return parent;
	}

	public TreeItem( Fun1<S, K> keyFun ) {
		this.keyFun = keyFun;
	}

	public TreeItem() {}


	public boolean hasParent() {
		return parent != null;
	}

	public S getChild( K key ) {
		return children.get( key );
	}

	@SafeVarargs
	public final S getChild( K... keys )
	{
		S s = thisAsS();
		for ( K key : keys ) {
			s = s.getChild( key );
		}
		return s;
	}

	@SuppressWarnings( "unchecked" )
	private S thisAsS() {
		return (S) this;
	}

	public S findChild( K key )
	{
		if ( Util.equals( key, this ) ) {
			return thisAsS();
		} else {
			for ( S s : children.values() ) {
				S ret = s.findChild( key );
				if ( ret != null ) return ret;
			}
			return null;
		}
	}

	public void removeSelf() {
		getParent().removeChild( thisAsS() );
	}

	public void removeChild( K c ) {
		children.remove( c );
	}

	public void removeChild( S c ) {
		children.remove( keyFun.run( c ) );
	}

	public void addChild( S c )
	{
		if ( Util.equals( children, Collections.emptyMap() ) ) {
			children = makeMap();
		}

		c.parent = thisAsS();
		c.keyFun = keyFun;
		children.put( keyFun.run( c ), c );
	}

	public void addChildren( Iterable<S> cs ) {
		for ( S s : cs ) addChild( s );
	}

	public void setKeyFun( Fun1<S, K> keyFun ) {
		this.keyFun = keyFun;
	}

	@Override
	public Collection<S> getChildren() {
		return children.values();
	}

	@SuppressWarnings( { "unchecked", "rawtypes" } )
	@Override
	public Iterator<S> iterator() {
		return (Iterator) new EntryIterator<S>( this, true );
	}

	protected Map<K, S> makeMap() {
		return new ArrayMap<K, S>();
	}

//	@SuppressWarnings( "unchecked" )
//	@Override
//	public TreeItem<K, S> clone()  {
//		TreeItem<K, S> result;
//		try {
//			result = (TreeItem<K, S>) super.clone();
//		} catch ( CloneNotSupportedException exc ) {
//			throw new RuntimeException( exc );
//		}
//
//		result.children = new ArrayMap<>();
//		result.children.putAll( children );
//
//		return result;
//	}

	@SuppressWarnings( "unchecked" )
	@Override
	public TreeItem<K, S> clone()
	{
		TreeItem<K, S> result;

		try {
			result = (TreeItem<K, S>) super.clone();
		} catch ( CloneNotSupportedException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}

		result.children = makeMap();

		for ( TreeItem<K, S> child : getChildren() ) {
			result.addChild( (S) child.clone() );
		}

		return result;
	}


}


class CommandData extends TreeItem<String, CommandData>
{
	String s;
	int i;
}
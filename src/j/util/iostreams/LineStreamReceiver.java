package j.util.iostreams;

import j.util.functional.Action1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LineStreamReceiver extends StreamReceiverBase
{
    private final Action1<String> handler;

    public LineStreamReceiver(
            Action1<String> errorInHandler, Action1<Object> errHandler ) {
        super( null, errHandler );
        this.handler = errorInHandler;
    }

    protected void runReadLoop() throws IOException
    {
        String line;
        BufferedReader br = new BufferedReader(
                new InputStreamReader( inStream ), 100 /* keep small for testing */);

        while ( !isStopped && ( line = br.readLine() ) != null ) {
            handler.run( line );
        }
    }

}

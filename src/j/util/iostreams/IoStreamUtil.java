package j.util.iostreams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class IoStreamUtil {

    private IoStreamUtil() {}
    
    public static StreamLineIterable lineIterable( final InputStream s ) {
        return new StreamLineIterable( s );
    }
    
    public static final class StreamLineIterable implements Iterable<String>, AutoCloseable {
        private InputStream stream;
        
        public StreamLineIterable( InputStream stream ) {
            this.stream = stream;
        }

        public Iterator<String> iterator() {
            return new StreamLineIterator( stream );
        }

        public void close() throws Exception {
            stream.close();
        }
    }
    
    private static final class StreamLineIterator implements Iterator<String> {
        private String nextLine = null;
        private BufferedReader reader;
        private boolean eof = false;
        
        public StreamLineIterator( InputStream s ) {
            reader = new BufferedReader( new InputStreamReader( s ) );
        }

        public boolean hasNext() {
            try {
                if ( !eof && nextLine == null ) {
                    nextLine = reader.readLine();
                    if ( nextLine == null ) {
                        eof = true;
                        reader.close();
                    }
                }
                return !eof;
            } catch ( IOException exc ) {
                throw new RuntimeException( exc );
            }
        }

        public String next() {
            if ( !hasNext() ) throw new NoSuchElementException();
            
            String ret = nextLine;
            nextLine = null;
            return ret;
        }

        public void remove() {
            throw new UnsupportedOperationException( "No remove on " + StreamLineIterator.class );
        }
    }

    
}

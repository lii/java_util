package j.util.iostreams;

import j.util.functional.Action1;
import j.util.util.Asserts;
import j.util.util.Util;

import java.io.IOException;
import java.io.InputStream;


public abstract class StreamReceiverBase implements StreamReceiver
{
	// stream to receive data from child
	protected InputStream inStream;

	protected final Action1<Object> errHandler;

	protected transient boolean
	    isClosed = false,  // Used to stop receiver and close stream
        isStopped = false; // Used to stop receiver without closing stream

	protected static Action1<Object> DEFAULT_ERR_HANDLER = new Action1<Object>() {
		public void run( Object arg ) {
			( (Exception) arg ).printStackTrace();
		}
	};

	public StreamReceiverBase( InputStream inStream, Action1<Object> errHandler )
	{
		this.inStream = inStream;
		this.errHandler = errHandler;
	}


	public StreamReceiverBase( Action1<Object> errHandler ) {
	    this( null, errHandler );
    }

    protected abstract void runReadLoop() throws IOException;

	/**
	 * Reads data from child and invokes handler with result.
	 */
    @Override
	public void run()
	{
		try {
		    runReadLoop();

		    if ( isClosed ) Util.close( inStream );

		} catch ( IOException exc ) {
			errHandler.run( exc );
		}
	}

	@Override
	public void close() {
	    stop();
		this.isClosed = true;
	}

    @Override
    public void stop() {
        this.isStopped = true;
    }



    @Override
    public void setStream( InputStream inStream ) {
        Asserts.state( inStream != null, "Can only set stream once" );
        this.inStream = inStream;
    }
}




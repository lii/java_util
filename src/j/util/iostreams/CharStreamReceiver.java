package j.util.iostreams;

import j.util.functional.Action1;

import java.io.IOException;
import java.io.InputStreamReader;

public class CharStreamReceiver extends StreamReceiverBase
{
    // stream to receive data from child
    protected final Action1<Character> charHandler;

    public CharStreamReceiver( Action1<Character> charHandler, Action1<Object> errHandler ) {
        super( null, errHandler );
        this.charHandler = charHandler;
    }

    protected void runReadLoop() throws IOException
    {
        final InputStreamReader reader = new InputStreamReader( inStream );
        int ch;

        while ( !isClosed && ( ch = reader.read() ) != -1 ) {
            charHandler.run( (char) ch );
        }
    }
}
package j.util.iostreams;

import java.io.Closeable;
import java.io.InputStream;

public interface StreamReceiver extends Runnable, Closeable {

    /**
     * Used to stop receiver without closing stream.
     */
    public void stop();

    /**
     * Used to stop receiver and close stream.
     */
    public void close();

    /**
     * The stream can only be set once.
     */
    public void setStream( InputStream inStream );

}
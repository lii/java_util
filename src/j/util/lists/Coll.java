package j.util.lists;

public interface Coll<E>
{
	public void push( E e );
	public E pop();
	public boolean isEmpty();
	public int size();
	public E peek();
}

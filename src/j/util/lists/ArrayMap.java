package j.util.lists;

import j.util.util.ArrayIterator;
import j.util.util.NotImplementedException;

import java.util.AbstractCollection;
import java.util.AbstractMap.SimpleEntry;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


/**
 * A Map backed by an array for minimum memory footprint and fast lookups for
 * small sizes.
 */
public class ArrayMap<K, V> implements Map<K, V>
{
	private Object[]
			keys = new Object[0],
			values = new Object[0];

	private int size = 0;

	@Override
	public Set<Entry<K, V>> entrySet() {
		return new ArrayMapEntrySet();
	}

	@Override
	public boolean containsKey( Object key )
	{
		return ArrayUtil.getIndex( keys, key ) != -1;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public void putAll( Map<? extends K, ? extends V> m )
	{
		for ( Entry<? extends K, ? extends V> e : m.entrySet() ) {
			put( e.getKey(), e.getValue() );
		}
	}

	@Override
	public void clear() {
		keys = new Object[ 0 ];
		values = new Object[ 0 ];
		size = 0;
	}

	@SuppressWarnings( "unchecked" )
	@Override
	public V remove( Object key )
	{
		int i = ArrayUtil.getIndex( keys, key );

		if ( i == -1 ) return null;

		Object v = values[ i ];

		for ( ; i < size - 1; i++ ) {
			keys[ i ] = keys[ i + 1 ];
			values[ i ] = values[ i + 1 ];
		}

		keys[ size -1 ] = null;
		values[ size - 1 ] = null;
		size--;

		return (V) v;
	}

	@SuppressWarnings( "unchecked" )
	@Override
	public V put( K key, V value )
	{
		int i = ArrayUtil.getIndex( keys, key );

		Object result = null;

		if ( i == -1 ) {
			i = size;
			size++;
			ensureSize( size );
		} else {
			result = values[ i ];
		}

		keys[ i ] = key;
		values[ i ] = value;

		return (V) result;
	}

	private void ensureSize( int s )
	{
		if ( s >= keys.length ) {
			values = ArrayUtil.realloc( values, keys.length + 1 );
			keys = ArrayUtil.realloc( keys, keys.length + 1 );
		}
	}

	@Override
	public Collection<V> values() {
		return new ArrayMapValues();
	}




	@Override
	public int hashCode()
	{
		int hash = 0;

		for ( int i = 0; i < size; i++ ) {
			Object k = keys[ i ];
			Object v = values[ i ];
			hash += ( k == null ? 0 : k.hashCode() )
					^ ( v == null ? 0 : v.hashCode() ) ;
		}

		return hash;
    }



	@SuppressWarnings( "unchecked" )
	@Override
	public boolean equals( Object o )
	{
		if ( o == this ) return true;
		if ( ! ( o instanceof Map ) ) return false;

		Map<K, V> m = (Map<K, V>) o;

		if ( m.size() != size() ) return false;

		try {
			for ( int i = 0; i < size; i++ ) {
				K key = (K) keys[ i ];
				V value = (V) values[ i ];

				if ( value == null ) {
					if ( !( m.get( key ) == null && m.containsKey( key ) ) )
						return false;
				} else {
					if ( !value.equals( m.get( key ) ) )
						return false;
				}
			}

		} catch ( ClassCastException e ) {
			return false;
		} catch ( NullPointerException exc ) {
			return false;
		}

		return true;
	}


	private class ArrayMapValues extends AbstractCollection<V>
	{

		@SuppressWarnings( "unchecked" )
		@Override
		public Iterator<V> iterator() {
			return new ArrayIterator<V>( (V[])  values, size );
		}

		@Override
		public int size() {
			return size;
		}

	}

	@Override
	public boolean containsValue( Object value ) {
		return ArrayUtil.getIndex( values, value ) != -1;
	}

	@Override public int size() {
		return size;
	}


	@SuppressWarnings( "unchecked" )
	@Override
	public V get( Object key ) {
		int i = ArrayUtil.getIndex( keys, key );
		return i == -1 ? null : (V) values[ i ];
	}

	@Override public Set<K> keySet() {
		return new ArrayKeySet();
	}


	private class ArrayKeySet extends AbstractSet<K> {

		@SuppressWarnings( "unchecked" )
		@Override
		public Iterator<K> iterator() {
			return new ArrayIterator<K>( (K[]) keys, size );
		}

		@Override
		public int size() {
			return size;
		}
	}


	private class ArrayMapEntrySet extends AbstractSet<Entry<K, V>>
	{
		@Override public Iterator<Entry<K, V>> iterator() {
			return new ArrayMapEntryIterator();
		}

		@Override public int size() {
			return size;
		}

	}

	private class ArrayMapEntryIterator implements Iterator<Entry<K, V>>
	{
		private int i;

		@Override
		public boolean hasNext() {
			return i < size;
		}

		@SuppressWarnings( "unchecked" )
		@Override
		public Entry<K, V> next() {
			if ( !hasNext() ) throw new NoSuchElementException();

			return new SimpleEntry<K, V>( (K) keys[ i ], (V) values[ i++ ] );

		}

		@Override public void remove() {
			throw new NotImplementedException();
		}

	}


    public String toString()
    {
        Iterator<Entry<K,V>> i = entrySet().iterator();
        if (! i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (;;) {
            Entry<K,V> e = i.next();
            K key = e.getKey();
            V value = e.getValue();
            sb.append(key   == this ? "(this Map)" : key);
            sb.append('=');
            sb.append(value == this ? "(this Map)" : value);
            if (! i.hasNext())
                return sb.append('}').toString();
            sb.append(',').append(' ');
        }
    }

}






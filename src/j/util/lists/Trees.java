package j.util.lists;

import j.util.eventhandler.Parent;
import j.util.functional.Pred2;
import j.util.tree.Tree;
import j.util.tree.TreeImpl;
import j.util.util.Util;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trees {

	public static <T> Tree<T> empty( Comparator<Object> comp ) {
		return new TreeImpl<T>( comp );
	}


	public static <T> TreeImpl<T> buildFromClass( Class<?> cls ) {
		try {
			return buildFromClassInner( cls );
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException( e );
		} catch ( IllegalAccessException e ) {
			throw new RuntimeException( e );
		}
	}

	private static <T> TreeImpl<T> buildFromClassInner( Class<?> cls ) throws IllegalArgumentException, IllegalAccessException
	{
		List<Field> sorted = new ArrayList<Field>();
		Lists.sortPartially( Arrays.asList( cls.getFields() ), DEPENDS_ON, sorted );
		Map<String, Object> nameObjMap = new HashMap<String, Object>();

		TreeImpl<T> tree = new TreeImpl<T>();


		for ( Field f : sorted ) {
			f.setAccessible( true );
			@SuppressWarnings( "unchecked" )
			T val = (T) f.get( null );
			String d = getParentAnnot( f );
			nameObjMap.put( f.getName(), val );

			if (  d.isEmpty() ) {
				tree.setData( val );
			} else {
				Object parentObj = nameObjMap.get( d );
				tree.findSubtree( parentObj ).add( val );
			}
		}

		return tree;
	}


	private static final Pred2<Field, Field> DEPENDS_ON = new Pred2<Field, Field>() {
		public Boolean run( Field arg1, Field arg2 ) {
			String parentAnnot = getParentAnnot( arg1 ),
				depName = arg2 == null ? "" : arg2.getName();

				return Util.equals( parentAnnot, depName );
		}
	};

	private static String getParentAnnot( AnnotatedElement c ) {
		return c.getAnnotation( Parent.class ).value();
	}
}







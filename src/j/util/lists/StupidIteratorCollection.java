package j.util.lists;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Stupid collection to use as a work around when an Collection or Iterable is
 * expexted, but all you have is an Iterator. As in a for loop.
 */
public class StupidIteratorCollection<E> extends AbstractCollection<E>
{
	private Iterator<E> itr;
	
	public StupidIteratorCollection( Iterator<E> itr ) {
	    this.itr = itr;
    }
	
	@Override
    public Iterator<E> iterator()
    {
		if ( itr == null ) throw new IllegalStateException();
		Iterator<E> ret = itr;
		itr = null;
	    return ret;
    }

	@Override public int size() {
		throw new UnsupportedOperationException();
	}

	@Override 
	public Object[] toArray()
	{
		List<Object> l = new LinkedList<Object>();
		
		while ( itr.hasNext() ) {
			l.add( itr.next() );
		}
		
	    return l.toArray();
    }
	
}

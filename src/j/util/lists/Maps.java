package j.util.lists;

import java.util.HashMap;
import java.util.Map;

public class Maps {
        
    @SuppressWarnings("unchecked")
    public static <K, V> Map<K, V> make(Object... keyValList) {
            Map<K, V> map = new HashMap<>();
            
            for (int i = 0; i < keyValList.length; i += 2) {
                map.put((K) keyValList[i], (V) keyValList[i + 1]);
            }
            
            return map;
        }

    @SuppressWarnings("unchecked")
    public static <K, V> HashMap<K, V> make(Object[][] data) {
        HashMap<K, V> map = new HashMap<K, V>();
        for (Object o : data) {
            Object[] e = (Object[]) o;
            map.put((K) e[0], (V) e[1]);
        }
        return map;
    }

}

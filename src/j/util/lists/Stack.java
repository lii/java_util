package j.util.lists;

public class Stack<E> implements Coll<E>
{
	private java.util.Stack<E> impl = new java.util.Stack<E>();

	@Override
    public boolean isEmpty() {
	    return impl.isEmpty();
    }

	@Override
    public E peek() {
	    return impl.peek();
    }

	@Override
    public E pop() {
	    return impl.pop();
    }

	@Override
    public void push( E e ) {
		impl.push( e );
    }

	@Override
    public int size() {
	    return impl.size();
    }
	
}

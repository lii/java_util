package j.util.lists;

import j.util.functional.Fun1;
import j.util.functional.Fun2;
import j.util.util.NotImplementedException;
import j.util.util.Util;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;



class Cons<E>
{
	public Cons<E> next;
	public E value;

	public Cons( E value, Cons<E> next ) {
        this.value = value;
        this.next = next;
    }

	public Cons<E> cons( E e ) {
		return Cons.make( e, this );
	}

	public static <E> Cons<E> make( E e, Cons<E> next ) {
		return new Cons<E>( e, next );
	}
	
	public String toString() {
	    return Util.simpleToString( this, value );
	}
}

public final class Li<E> extends AbstractCollection<E> implements List<E> 
{
	@Override
    public boolean addAll( int index, Collection<? extends E> c ) {
        throw new NotImplementedException();
    }

    @Override
    public E set( int index, E element ) {
        throw new NotImplementedException();
    }

    @Override
    public void add( int index, E element ) {
        throw new NotImplementedException();
    }

    @Override
    public E remove( int index ) {
        throw new NotImplementedException();
    }

    @Override
    public int indexOf( Object o ) {
        throw new NotImplementedException();
    }

    @Override
    public int lastIndexOf( Object o ) {
        throw new NotImplementedException();
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new NotImplementedException();
    }

    @Override
    public ListIterator<E> listIterator( int index ) {
        throw new NotImplementedException();
    }

    @Override
    public List<E> subList( int fromIndex, int toIndex ) {
        throw new NotImplementedException();
    }

    private Cons<E> head;

	private Li( Cons<E> head ) {
        this.head = head;
    }

	public Li() {
		head = null;
	}

	@SuppressWarnings( "unchecked" )
	public static <E> Li<E> empty() {
		return ( Li<E> ) EMPTY;
	}

	@SuppressWarnings( { "rawtypes" } )
    public static final Li EMPTY = new Li();

	public Li<E> cons( E elem ) {
		return new Li<E>( Cons.make( elem, head ) );
	}

	public E head() {
		return head.value;
	}

	@Override
    public boolean isEmpty() {
		return head == null;
	}

	@Override public Iterator<E> iterator() {
		return new LiIterator<E>( head );
	}

    public E get( int index ) {
		int i = 0;
		Cons<E> e = head;
		while ( e != null ) {
			if ( i == index ) return e.value;
			i++;
			e = e.next;
		}
//		for ( E e : this ) {
//			if ( i == index ) return e;
//			i++;
//		}
		throw new NoSuchElementException();
    }

	@Override
    public int size()
	{
		int i = 0;
		Cons<E> e = head;
		while ( e != null ) {
        	i++;
			e = e.next;
		}
//        for ( E e : this ) {
//        	i++;
//        }
        return i;
    }

//	public Li<E> init( int n ) {
//	    Li<E> l = new Li<E>( head );
//        while ( e != null ) {
//            if ( index == 0 ) return new Li<E>( e );
//            index--;
//            e = e.next;
//        }
//
//	}

	public Li<E> tail() {
		if ( head == null ) throw new NoSuchElementException();
		return new Li<>( head.next );
	}
	

	public Li<E> drop( int nr )
	{
		Cons<E> e = head;
		while ( e != null ) {
			if ( nr == 0 ) return new Li<E>( e );
			nr--;
			e = e.next;
		}
//		for ( Cell<E> e : cellColl() ) {
//			if ( index == 0 ) {
//				return new Li<>( e );
//			}
//			index--;
//		}
		throw new NoSuchElementException("Dropping " + nr + " but Li is shorter" );
	}

	public <E2> Li<E2> map( Fun1<? super E, E2> f ) {
		Li<E2> l = new Li<E2>();
//		for ( E e : this ) {
//			l.dInsert( f.run( e ) );
//		}
		Cons<E> h = head;
		while ( h != null ) {
			l.insertDest( f.run( h.value ) );
			h = h.next;
		}
		l.reverseDest();
		return l;
	}

	public <A> A fold( Fun2<? super E, ? super A, ? extends A> f, A acc )
	{
//		for ( E e : this ) {
//			acc = f.run( e, acc );
//		}
		Cons<E> h = head;
		while ( h != null ) {
			acc = f.run( h.value, acc );
			h = h.next;
		}
		return acc;
	}

	public Li<E> filter( Fun1<? super E, Boolean> f )
	{
		Li<E> l = new Li<E>();
//		for ( E e : this ) {
//			if ( f.run( e ) ) {
//				l.dInsert( e );
//			}
//		}

		Cons<E> h = head;
		while ( h != null ) {
			if ( f.run( h.value ) ) {
				l.insertDest( h.value );
			}
			h = h.next;
		}
		l.reverseDest();
		return l;
	}





	public E find( Fun1<? super E, Boolean> f ) {
		Cons<E> e = head;
		while ( e != null ) {
			if ( f.run( e.value ) ) return e.value;
			e = e.next;
		}
//		for ( E e : this ) {
//			if ( f.run( e ) ) return e;
//		}
		throw new NoSuchElementException();
	}

	public E find( Fun1<? super E, Boolean> f, E d ) {
		Cons<E> e = head;
		while ( e != null ) {
			if ( f.run( e.value ) ) return e.value;
			e = e.next;
		}
//		for ( E e : this ) {
//			if ( f.run( e ) ) return e;
//		}
		return d;
	}

	public void insertDest( E e ) {
		head = Cons.make( e, head );
	}

	public Li<E> reverse()
	{
		Li<E> l = new Li<E>();
		Cons<E> e = head;
		while ( e != null ) {
			l.insertDest( e.value );
			e = e.next;
		}
//		for ( E e : this ) {
//			l.dInsert( e );
//		}
		return l;
	}

	public static <E> Li<E> make( E elem ) {
		Li<E> l = new Li<E>();
		l.insertDest( elem );
		return l;
	}

	@SafeVarargs
	public static <E> Li<E> make( E... elems )
	{
		Li<E> l = new Li<>();
		for ( int i = elems.length - 1; i >= 0; i-- ) {
			l.insertDest( elems[i] );
		}
		return l;
	}

	public void reverseDest()
	{
	    // TODO: Attempt to write this without iterator:
        if ( head == null ) return;

        Cons<E> cur = head.next;
        Cons<E> prev = head;
        Cons<E> temp;

        while ( cur != null ) {
            temp = cur.next;
            cur.next = prev;
            prev = cur;
            cur = temp;
        }

        head.next = null;
        head = prev;

//		if ( head == null ) return;
//		Cons<E> end = head;
//		for ( Cons<E> e : cellColl() ) {
//			e.next = head;
//			head = e;
//		}
//		end.next = null;
	}

	@SuppressWarnings( "unused" )
    private Iterable<Cons<E>> cellColl() {
		return new StupidIteratorCollection<>( new CellIterator<>( head ) );
	}

	/**
	 * TODO: NOT TESTED!
	 */
	@Override
	public boolean equals( Object o )
	{
	    if ( !(o instanceof Iterable) ) return false;

	    Iterable<?> other = (Iterable<?>) o;
	    Cons<E> e = head;

	    for ( Object oe : other ) {
            if ( e == null || !Objects.equals( e.value, oe ) ) return false; // This shorter than other
	        e = e.next;
	    }

	    if ( e != null ) return false; // other shorter than this

	    return true;
	}


}



class CellIterator<E> implements Iterator<Cons<E>>
{
	private Cons<E> next;

	public CellIterator( Cons<E> next ) {
        this.next = next;
    }

	@Override public boolean hasNext() {
        return next != null;
    }

	@Override public Cons<E> next() {
		if ( !hasNext() ) throw new NoSuchElementException();
		Cons<E> ret = next;
		next = next.next;
        return ret;
    }

	@Override public void remove() {
		throw new UnsupportedOperationException();
    }
}


class LiIterator<E> implements Iterator<E>
{
	private Cons<E> next;

	public LiIterator( Cons<E> next ) {
        this.next = next;
    }

	@Override public boolean hasNext() {
        return next != null;
    }

	@Override public E next() {
		if ( !hasNext() ) throw new NoSuchElementException();
		E ret = next.value;
		next = next.next;
        return ret;
    }

	@Override public void remove() {
		throw new UnsupportedOperationException();
    }
}

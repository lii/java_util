package j.util.lists;

import j.util.util.Asserts;

import java.util.Iterator;

class SeqIterator implements Iterator<Integer>
{

	private int step, i, value;

	public SeqIterator( int start, int end ) {
		this( start, end, 1 );
	}

	public SeqIterator( int start, int end, int step )
	{
		if ( start > end ) {
			Asserts.arg( step < 0, "step" );
		} else {
			Asserts.arg( step > 0, "step" );
		}

		int range = end - start;

		this.value = start;
		this.step =  step;
		this.i = Math.abs( range / step ) + (step > 0 ? 1 : 0);
		throw new IllegalStateException( "Not tested!" );
	}


	@Override
    public boolean hasNext() {
	    return i > 0;
    }

	@Override
    public Integer next() {
		int ret = value;
		i--;
		value += step;
	    return ret;
    }

	@Override
    public void remove() {
		throw new UnsupportedOperationException();
    }
}
package j.util.lists;

import java.util.Iterator;


public class IteratorContainer<T, I extends Iterator<T>> implements Iterable<T> {

	private I itr;

	public IteratorContainer() {
		super();
	}

	public IteratorContainer( I itr ) {
		this.itr = itr;
	}

	@Override
	public I iterator() {
		return itr;
	}

}

package j.util.lists;

import java.util.Iterator;
import java.util.List;

/**
 * Iterates over a list of list as if it was a list.
 */
public class FlatIterator<E> implements Iterator<E>
{
	private Iterator<E> inner;
	private Iterator<List<E>> outer;
	
	public FlatIterator( List<List<E>> list )
	{
		outer = list.iterator();
	}
	
	@Override
    public boolean hasNext() {
		return outer.hasNext();
    }
	
	@Override
    public E next()
    {
    	E result;
    	
		if ( hasNext() ) {
    		result = inner.next();
    	} else {
    		throw new IndexOutOfBoundsException();
    	}
		
    	while ( !inner.hasNext() ) {
			if ( !hasNext() ) {
    			break;
    		}
    		inner = outer.next().iterator();
    	}
    	
    	return result;
    }

	@Override
    public void remove() {
	    throw new UnsupportedOperationException();
    }
	
}

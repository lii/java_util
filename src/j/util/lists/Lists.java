package j.util.lists;

import j.util.functional.Fun2;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Queue;


public final class Lists
{
	public static final Iterator<Object> NULL_ITERATOR = new Iterator<Object>() {
	    public boolean hasNext() {
	        return false;
	    }
	    public Object next() {
	        throw new NoSuchElementException( "next() called on a null-iterator" );
	    }
	    public void remove() {
	        throw new UnsupportedOperationException( "remove() not supported on null-iterator" );
	    }
	};

	private Lists() {}

	public static Iterable<Integer> seq( int start, int end, int step ) {
		return new StupidIteratorCollection<Integer>( new SeqIterator( start, end, step ) );
	}

	public static Iterable<Integer> seq( int start, int end ) {
		return seq( start, end, 1);
	}

	@SuppressWarnings( "unchecked" )
    public static <T> List<T> nullToEmpyList( List<T> l ) {
		return l != null ? l : Collections.EMPTY_LIST;
	}

	/**
	 * Sorts a list so that some objects comes before objects that are dependant
	 * on them.
	 *
	 * For every object in the list for which dependsOn returns true for the
	 * second object, that object is ordred before the other.
	 */
	public static <T> void sortPartially( Iterable<T> list, Fun2<? super T, ? super T, Boolean> dependsOn, Collection<? super T> result )
	{
		Queue<T> currentList = new LinkedList<T>();
		currentList.add( null );

		while ( !currentList.isEmpty() ) {
			T current = currentList.poll();
			for ( T dependant : list ) {
				if ( dependsOn.run( dependant, current ) ) {
					result.add( dependant );
					currentList.add( dependant );
				}
			}
		}
	}

	/**
	 * Makes a shallow copy of a two dimensionaly array.
	 */
	public static Integer[][] makeSquareCopy( Integer[][] source )
	{
		Integer[][] ret = new Integer[source.length][source[0].length];

		for ( int i = 0; i < source.length; i++ ) {
			ret[i] = Arrays.copyOf( source[i], source.length );
		}

		return ret;
	}

	@SuppressWarnings("unchecked")
	public static <T> Iterator<T> makeNullIterator()
	{
		return (Iterator<T>) Lists.NULL_ITERATOR;
	}

	public static void moveArrayDataDown( int[] arr, int start, int end )
	{
		for ( int i = start; i > end; i-- ) {
			arr[i] = arr[i - 1];
		}
	}

	public static void moveArrayDataUp( int[] arr, int start, int end )
	{
		for ( int i = start; i < end; i++ ) {
			arr[i] = arr[i + 1];
		}
	}

	public static boolean iterEquals( Iterator<?> i1, Iterator<?> i2 ) {
		while ( i1.hasNext() && i2.hasNext() ) {
			if (!Objects.equals( i1.next(), i2.next() ) ) return false;
		}
		return !i1.hasNext() && !i2.hasNext();
	}

	public static boolean iterEquals( Iterable<?> it1, Iterable<?> it2 ) {
		return iterEquals( it1.iterator(), it2.iterator() );
	}

	public static <T> List<T> iterToList( Iterator<T> itr )
	{
		List<T> l = new LinkedList<T>();
		while ( itr.hasNext() ) {
			l.add( itr.next() );
		}
	    return l;
	}

	public static <T> List<T> iterToList( Iterable<T> itr ) {
		return iterToList( itr.iterator() );
	}

	public static int deepHashCode(Object a[])
	{
	    if (a == null) return 0;

	    int result = 1;

		for ( int i = 0; i < a.length; i++ ) {

	    	Object element = a[i];

	    	int elementHash;

	        if (element instanceof Object[]) {
	            elementHash = deepHashCode((Object[]) element);
	        } else if (element != null) {
	            elementHash = element.hashCode();
	        } else {
	        	elementHash = 0;
	        }

	        result = 31 * result + elementHash;
	    }

	    return result;
	}

	public static boolean deepEquals(Object[] a1, Object[] a2)
	{
		if ( a1 == a2 ) return true;
		if ( a1 == null || a2 == null ) return false;
		if ( a2.length != a1.length ) return false;

		for ( int i = 0; i < a1.length; i++ ) {
			final Object e1 = a1[i];
			final Object e2 = a2[i];

			if ( e1 == e2 ) continue;
			if ( e1 == null ) return false;

			// Figure out whether the two elements are equal
			final boolean eq;

			if ( e1 instanceof Object[] && e2 instanceof Object[] ) {
				eq = deepEquals( (Object[]) e1, (Object[]) e2 );
			} else {
				eq = e1.equals( e2 );
			}

			if ( !eq ) return false;
		}
		return true;
	}
}
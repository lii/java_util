
package j.util.lists;

import j.util.util.Asserts;

import java.util.Iterator;



/**
 * Iterator that iterates over every element in a two dimensional array.
 * Collumn first.
 * 
 * Does not support arrays of length 0, either as rows or cols 
 */
public class SqareArrayIterator<T> implements Iterator<T>
{
	private T[][] data;
	private int col = -1, row = 0;
	
	public SqareArrayIterator( T[][] data )
	{
		Asserts.arg( data.length != 0, "Arrays of length 0 is not supported" );
		Asserts.arg( data[0].length != 0, "Arrays of length 0 is not supported" );
		
		this.data = data;
	}
	
	public final int getRow() {
		return row;
	}
	
	public final int getCol() {
		return col;
	}
	
	@Override 
	public boolean hasNext()
	{
		return (col < data.length - 1) || ( row < data.length - 1 );
	}

	@Override
	public final T next()
	{
		col++;
	
		if ( col >= data[row].length ) {
			col = 0;
			row++;
		}
		
        return current();
    }
	
	public final T current()
	{
		return data[row][col];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException( getClass() + " does not support remove" );
    }
	
	
}






class ItrTester
{
	
	
	
	
	
	
	
	
	
	public static void main( String[] args )
	{
		
	}
}


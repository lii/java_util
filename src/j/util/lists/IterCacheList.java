package j.util.lists;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * A wrapper list that delegates to another this. It caches the lists interator,
 * so that it can be reused.
 */
public class IterCacheList<T> implements List<T> {

	private IndexIterator<T> itr;

	public IterCacheList( List<T> list ) {
		this.itr = new IndexIterator<T>( list );
	}

	public IterCacheList() {
		itr = new IndexIterator<T>();
	}

	public void setList( List<T> list ) {
		itr.setList( list );
	}

	public IterCacheList<T> withList( List<T> list ) {
		setList( list );
		return this;
	}

	@Override
	public Iterator<T> iterator() {
		itr.setToStart();
		return itr;
	}

	public void removeCurrent() {
		itr.remove();
	}

	///////////////////////////////

	public int size() {
		return itr.getList().size();
	}

	public boolean isEmpty() {
		return itr.getList().isEmpty();
	}

	public boolean contains( Object o ) {
		return itr.getList().contains( o );
	}

	public Object[] toArray() {
		return itr.getList().toArray();
	}

	public <E> E[] toArray( E[] a ) {
		return itr.getList().toArray( a );
	}

	public boolean add( T e ) {
		return itr.getList().add( e );
	}

	public boolean remove( Object o ) {
		return itr.getList().remove( o );
	}

	public boolean containsAll( Collection<?> c ) {
		return itr.getList().containsAll( c );
	}

	public boolean addAll( Collection<? extends T> c ) {
		return itr.getList().addAll( c );
	}

	public boolean addAll( int index, Collection<? extends T> c ) {
		return itr.getList().addAll( index, c );
	}

	public boolean removeAll( Collection<?> c ) {
		return itr.getList().removeAll( c );
	}

	public boolean retainAll( Collection<?> c ) {
		return itr.getList().retainAll( c );
	}

	public void clear() {
		itr.getList().clear();
	}

	public boolean equals( Object o ) {
		return itr.getList().equals( o );
	}

	public int hashCode() {
		return itr.getList().hashCode();
	}

	public T get( int index ) {
		return itr.getList().get( index );
	}

	public T set( int index, T element ) {
		return itr.getList().set( index, element );
	}

	public void add( int index, T element ) {
		itr.getList().add( index, element );
	}

	public T remove( int index ) {
		return itr.getList().remove( index );
	}

	public int indexOf( Object o ) {
		return itr.getList().indexOf( o );
	}

	public int lastIndexOf( Object o ) {
		return itr.getList().lastIndexOf( o );
	}

	public ListIterator<T> listIterator() {
		return itr.getList().listIterator();
	}

	public ListIterator<T> listIterator( int index ) {
		return itr.getList().listIterator( index );
	}

	public List<T> subList( int fromIndex, int toIndex ) {
		return itr.getList().subList( fromIndex, toIndex );
	}






}

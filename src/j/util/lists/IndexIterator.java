package j.util.lists;

import java.util.Iterator;
import java.util.List;

/**
 * An iterator that stores an intex it uses to access the list it operates on.
 *
 * It can change which list it operates on.
 */
public class IndexIterator<T> implements Iterator<T> {

	private List<T> list;
	private int i;

	public void setToStart() {
		setIndex( 0 );
	}

	public void setList( List<T> l ) {
		list = l;
		setIndex( 0 );
	}

	public List<T> getList() {
		return list;
	}

	public void setIndex( int newI ) {
		i = newI;
	}

	public IndexIterator( List<T> list ) {
		this.list = list;
	}

	public IndexIterator() {
	}


	@Override
	public boolean hasNext() {
		return i < list.size();
	}

	@Override
	public T next() {
		return list.get( i++ );
	}

	@Override
	public void remove() {
		list.remove( --i );
	}

//	@Override
//	protected T computeNext()
//	{
//		if ( list.size() > i ) {
//			return list.get( i++ );
//		} else {
//			return endOfData();
//		}
//	}

}

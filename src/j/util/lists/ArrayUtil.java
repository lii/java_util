package j.util.lists;

import j.util.util.Util;

public final class ArrayUtil {
	ArrayUtil() {}

	public static int getIndex( Object[] arr, Object key )
	{
		for ( int i = 0; i < arr.length; i++ ) {
			if ( Util.equals( arr[ i ], key ) ) return i;
		}
		return -1;
	}

	public static void remove( Object[] arr, int i )
	{
		for ( ; i < arr.length - 1; i++ ) {
			arr[ i ] = arr[ i + 1 ];
		}

		arr[ arr.length ] = null;
	}


	public static Object[] realloc( Object[] os, int newSize )
	{
		Object[] newOs = new Object[ newSize ];
		System.arraycopy( os, 0, newOs, 0, os.length );
		return newOs;
	}


}

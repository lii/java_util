package j.util.lists;

import java.util.ArrayDeque;

public class Queue<E> implements Coll<E>
{
	private java.util.Queue<E> impl = new ArrayDeque<E>();
	
	@Override
    public boolean isEmpty() {
	    return impl.isEmpty();
    }

	@Override
    public E pop() {
	    return impl.poll();
    }
	
	@Override
    public E peek() {
	    return impl.peek();
    }

	@Override
    public void push( E e ) {
		impl.add( e );
    }

	@Override
    public int size() {
	    return impl.size();
    }
	
}

package j.util.caller;

import j.util.functional.Action0;

public class BasicCaller0 extends AbstractCaller<Action0>
{
	public void call() {
		for ( Action0 action : listenerList ) {
			action.run();
		}
	}
}

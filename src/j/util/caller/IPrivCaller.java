package j.util.caller;

public interface IPrivCaller<E>
{
	public void call( E arg );
}

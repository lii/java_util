package j.util.caller;

import j.util.functional.Action1;

public interface IBasicCaller<T> extends Caller<Action1<T>> {
	
}

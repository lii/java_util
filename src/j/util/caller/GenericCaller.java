package j.util.caller;

import j.util.util.Asserts;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GenericCaller<E, T> extends AbstractCaller<T> implements IPrivCaller<E> {

	private final Method method;

	public GenericCaller( Class<E> parCls, Class<T> lisCls )
	{
		Asserts.arg( lisCls.isInterface(), "Only interfaces" );
		Method[] ms = lisCls.getMethods();
		Asserts.argEqual( ms.length, 1, "Wrong mumber of methods." );
		method = ms[0];
		Class<?>[] pars = method.getParameterTypes();
		Asserts.argEqual( pars.length, 1, "Wrong number of parameters." );
		Asserts.arg( pars[0].isAssignableFrom( parCls ), "Wrong type" );
	}

	@Override
	public void call( E arg )
	{
		try {
			for ( T lis : listenerList ) {
						method.invoke( lis, arg );
			}
		} catch ( IllegalArgumentException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		} catch ( IllegalAccessException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		} catch ( InvocationTargetException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}

	}

}

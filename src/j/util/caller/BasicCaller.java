package j.util.caller;

import j.util.functional.Action1;

public class BasicCaller<ARG_T> extends AbstractCaller<Action1<? super ARG_T>> 
								implements IPrivCaller<ARG_T>
{
	public void call( ARG_T arg ) {
		for ( Action1<? super ARG_T> action : listenerList ) {
			action.run( arg );
		}
	}
}

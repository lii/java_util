package j.util.caller;

import j.util.functional.Action3;

public class BasicCaller3<T1, T2, T3> extends AbstractCaller<Action3<T1, T2, T3>>
{
	public void call( T1 arg1, T2 arg2, T3 arg3 ) {
		for ( Action3<T1, T2, T3> action : listenerList ) {
			action.run( arg1, arg2, arg3 );
		}
	}
}

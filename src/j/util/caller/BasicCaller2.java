package j.util.caller;

import j.util.functional.Action2;

public class BasicCaller2<T1, T2> extends AbstractCaller<Action2<T1, T2>>
{
	public void call( T1 arg1, T2 arg2 ) {
		for ( Action2<T1, T2> action : listenerList ) {
			action.run( arg1, arg2 );
		}
	}
}

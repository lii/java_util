package j.util.caller;

public interface Caller<T>
{
	public abstract void addListener( T lis );
	public abstract boolean removeListener( T lis );
}

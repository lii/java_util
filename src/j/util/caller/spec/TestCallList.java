package j.util.caller.spec;

import static java.lang.System.out;

import j.util.caller.BasicCaller;
import j.util.caller.Caller;
import j.util.functional.Action1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;




class Foo
{
	public ActionCaller actionCaller = new ActionCaller();
	private BasicCaller<Integer> valueChangedCaller = new BasicCaller<Integer>();
	
	public void addActionListener(ActionListener l) {
		actionCaller.addListener( l );
	}

	public void addValueChangedListener(Action1<? super Integer> l) {
		valueChangedCaller.addListener( l );
	}
	
	public void somethingHappened() {
	    actionCaller.call( new ActionEvent( null, 1, "Hm." ) );
		valueChangedCaller.call( 3 );
	}

    public Caller<ActionListener> getActionCaller() {
    	return actionCaller;
    }
}



public class TestCallList
{
	public TestCallList()
	{
		
	}
	
	private javax.swing.Action sayAction = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override public void actionPerformed( ActionEvent e ) {
        	out.println( "Woo? " + e );
        }
	};
	
	private Action1<Integer> doValueChanged = new Action1<Integer>() {
        public void run( Integer arg ) {
        	out.println( "Nytt hm: " + arg );
        }
	};
	

	
	private Action1<Number> doNumberChanged = new Action1<Number>() {
        public void run( Number arg ) {
        	out.println( "Nytt hm: " + arg );
        }
	};

	public void run()
	{
		Foo foo = new Foo();
		
		foo.addActionListener( sayAction );
		foo.addValueChangedListener( doValueChanged );
		foo.addValueChangedListener( doNumberChanged );
		
		foo.somethingHappened();
	}
	
	
	public void main( String[] args )
	{
		out.println( "Starting." );
		
		TestCallList prg = new TestCallList();
		prg.run();
		
		out.println( "Terminating." );
	}
}

package j.util.caller.spec;

import j.util.functional.Action2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Callers
{
	private Callers() {}
	
	public static DelCaller<ActionListener, ActionEvent> newActionCaller()
	{
		return new DelCaller<ActionListener, ActionEvent>( 
			new Action2<ActionListener, ActionEvent>() {
				@Override public void run( ActionListener arg1, ActionEvent arg2 ) {
					arg1.actionPerformed( arg2 );
	            }
			});
	}
}

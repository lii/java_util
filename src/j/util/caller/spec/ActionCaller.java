package j.util.caller.spec;

import j.util.caller.AbstractCaller;
import j.util.caller.IPrivCaller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



class ActionCaller extends AbstractCaller<ActionListener>
				   implements IPrivCaller<ActionEvent>
{
	@Override
	public void call( ActionEvent event ) {
		for ( ActionListener lis : listenerList ) {
			lis.actionPerformed( event );
		}
	}
}

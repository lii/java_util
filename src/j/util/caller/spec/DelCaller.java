package j.util.caller.spec;

import j.util.caller.AbstractCaller;
import j.util.caller.IPrivCaller;
import j.util.functional.Action2;

public class DelCaller<LIS_T, E> extends AbstractCaller<LIS_T> implements IPrivCaller<E>
{
	private Action2<LIS_T, E> callAction;
	
	public DelCaller( Action2<LIS_T, E> callAction ) {
	    this.callAction = callAction;
    }

	@Override
    public void call( E arg ) {
		for ( LIS_T lis : listenerList ) {
			callAction.run( lis, arg );
		}
    }
}

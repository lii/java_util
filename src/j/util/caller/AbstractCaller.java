package j.util.caller;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class AbstractCaller<T> implements Caller<T>
{
	protected List<T> listenerList = new CopyOnWriteArrayList<T>();

    public Iterable<T> iterator()
    {
		return  listenerList;
    }

	public void clear() {
		listenerList.clear();
	}

	public void addListener( T lis ) {
		listenerList.add( lis );
	}

	public boolean removeListener( T lis ) {
		return listenerList.remove( lis );
	}
}

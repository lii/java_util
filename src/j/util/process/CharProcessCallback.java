package j.util.process;

import j.util.functional.Action1;
import j.util.iostreams.CharStreamReceiver;
import j.util.iostreams.StreamReceiverBase;

public abstract class CharProcessCallback extends ProcessCallback{

    @Override
    StreamReceiverBase createErrorStreamReceiver()
    {
        return new CharStreamReceiver(
                new Action1<Character>() {
                    public void run( Character arg ) { receiveStderr( arg ); }
                },
                new Action1<Object>() {
                    public void run( Object arg ) { handleError( arg ); }
                }
            );
    }

    @Override
    StreamReceiverBase createOutStreamReceiver()
    {
        return new CharStreamReceiver(
                new Action1<Character>() {
                    public void run( Character arg ) { receiveStdout( arg ); }
                },
                new Action1<Object>() {
                    public void run( Object arg ) { handleError( arg ); }
                } );
    }


    /**
     * Called when a line of text is written to the started process stdout
     * Called on a separate thread.
     */
    public void receiveStdout( char ch ) {}
    /**
     * Called when a line of text is written to the started process stderr.
     * Called on a separate thread (same as for receiveOutput if
     * redirectErrorStream is set to true).
     */
    public void receiveStderr( char ch ) {}
}
package j.util.process;

import java.io.IOException;

class PipeCallback extends CharProcessCallback
{
    private final ProcessHandler processHandler;


    public PipeCallback( ProcessHandler processHandler ) {
        this.processHandler = processHandler;
    }

    @Override
    public void receiveStdout( char ch )
    {
        try {
            processHandler.write( ch );
        } catch ( IOException exc ) {
            handleError( exc );
        }
    }

    @Override
    public void receiveStderr( char ch )
    {
        // Ignore stderr
//        try {
//            processHandler.write( ch );
//        } catch ( IOException exc ) {
//            handleError( exc );
//        }
    }

    @Override
    public void signalTerminated( int code ) {
        processHandler.close();
    }
}
package j.util.process;

import j.util.util.Asserts;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Convenience class for runnig a command in a blocking way and collecting the
 * output as String lists afterwards.
 */
public class ProcessRunner
{
	private ProcessHandler handler;
	private boolean isFinished = false;
	private ProcessBuilder builder;

	private List<String> output = new ArrayList<String>(),
		error = new ArrayList<String>();

	private ProcessCallback callback = new LineProcessCallback() {
		@Override
		public void receiveStdout( String line ) {
			output.add( line );
		}
		@Override
		public void receiveStderr( String line ) {
			error.add( line );
		}
	};


	public ProcessRunner( ProcessBuilder builder ) {
		this.builder = builder;
		handler = new ProcessHandler( builder, callback  );
	}

	public ProcessRunner( File workDir, String... cmdAndArgs )
	{
		this( new ProcessBuilder( cmdAndArgs ) );
		builder.directory( workDir );
		builder.redirectErrorStream( true );
	}

	public ProcessRunner( String... cmdAndArgs )
	{
		this( new ProcessBuilder( cmdAndArgs ) );
		builder.redirectErrorStream( true );
	}


	public List<String> getOutput() {
		Asserts.state( isFinished, "Cant get output until process has terminated" );
		return output;
	}

	public List<String> getError() {
		Asserts.state( isFinished, "Cant get output until process has terminated" );
		return error;
	}


	public void run() throws IOException {
		handler.run();
		isFinished = true;
	}
}

package j.util.process;

import j.util.iostreams.StreamReceiverBase;

public abstract class ProcessCallback
{
    public static final ProcessCallback
        EMPTY = new EmptyProcessCallback(),
        STDOUT = new StdProcessCallback();


    abstract StreamReceiverBase createErrorStreamReceiver();
    abstract StreamReceiverBase createOutStreamReceiver();

	/**
	 * Called when the process has terminated. On separate wait thread if
	 * runAsync is set to true, else on calling thread.
	 */
	public void signalTerminated( int code ) {}

	public void handleError( Object err ) {}

	private static class EmptyProcessCallback extends CharProcessCallback {};

    private static class StdProcessCallback extends LineProcessCallback {

        @Override public void receiveStdout( String line ) {
            System.out.println( line );
        }

        @Override public void receiveStderr( String line ) {
            System.err.println( line );
        }

        @Override
        public void handleError( Object err ) {
            ((Exception) err).printStackTrace();
        }
    };

}
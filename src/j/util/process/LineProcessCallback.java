package j.util.process;

import j.util.functional.Action1;
import j.util.iostreams.LineStreamReceiver;
import j.util.iostreams.StreamReceiverBase;

/**
 * Used to receive messages from a {@link ProcessHandler}.
 */
public abstract class LineProcessCallback extends ProcessCallback {

    @Override
    StreamReceiverBase createErrorStreamReceiver()
    {
        return new LineStreamReceiver(
                new Action1<String>() {
                    public void run( String arg ) { receiveStderr( arg ); }
                },
                new Action1<Object>() {
                    public void run( Object arg ) { handleError( arg ); }
                } );
    }

    @Override
    StreamReceiverBase createOutStreamReceiver()
    {
        return new LineStreamReceiver(
                new Action1<String>() {
                    public void run( String arg ) { receiveStdout( arg ); }
                },
                new Action1<Object>() {
                    public void run( Object arg ) { handleError( arg ); }
                } );
    }

    /**
     * Called when a line of text is written to the started process stdout
     * Called on a separate thread.
     */
    public void receiveStdout( String line ) {}
    /**
     * Called when a line of text is written to the started process stderr.
     * Called on a separate thread (same as for receiveOutput if
     * redirectErrorStream is set to true).
     */
    public void receiveStderr( String line ) {}
}
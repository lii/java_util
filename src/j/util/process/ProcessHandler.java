package j.util.process;

import static java.lang.System.out;
import j.util.iostreams.StreamReceiver;
import j.util.util.Asserts;
import j.util.util.Util;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Handles interaction with a child process running a command.
 *
 * Listens to input from the child in separate threads and calls a callback
 * when there is some.
 *
 * Closes streams and threads when process terminates.
 */
public class ProcessHandler implements Cloneable
{
	private Thread
		stdinThread,
		stderrThread,
		waitThread;

	private transient boolean started = false;

	private Process process;
	private ProcessBuilder builder;

	private ProcessCallback callback;

	private boolean redirectErrorStream;

	private transient StreamReceiver
	    stderrReceiver,
	    stdoutReceiver;

    private OutputStreamWriter outWriter;

	public ProcessHandler( ProcessBuilder builder, ProcessCallback callback )
	{
		this.builder = builder;
		this.callback = callback;
		this.redirectErrorStream = builder.redirectErrorStream();
	}

    public ProcessHandler( ProcessCallback callback, String... cmd )
    {
        this( new ProcessBuilder( cmd ), callback );
    }

    public ProcessHandler( ProcessCallback callback, List<String> cmd )
    {
        this( new ProcessBuilder( cmd ), callback );
    }

	public ProcessHandler( List<String> cmd ) {
	    this.builder = new ProcessBuilder( cmd );
    }

    public void setCallback( ProcessCallback callback ) {
        this.callback = callback;
    }

    public ProcessCallback pipeCallback() {
	    return new PipeCallback( this );
	}

	public Process getProcess() {
		return process;
	}

	private String makeName( String postfix ) {
	    return getClass().getSimpleName() + " " + builder.command().get( 0 ) + " " + postfix;
	}

	private void start() throws IOException
	{
        Asserts.state( !started, "Process allready started" );
        process = builder.start();
        started = true;

        outWriter = new OutputStreamWriter( process.getOutputStream() );

        // Setup stdio reader
        stdoutReceiver = callback.createOutStreamReceiver();
        stdoutReceiver.setStream( process.getInputStream() );

        stdinThread = new Thread( stdoutReceiver );
        stdinThread.setName( makeName( "stdin handler" ) );
        stdinThread.start();

        // Setup stderr reader (if we dont use inThread for that also)
        if ( !redirectErrorStream ) {
            stderrReceiver = callback.createErrorStreamReceiver();
            stderrReceiver.setStream( process.getErrorStream() );

            stderrThread = new Thread( stderrReceiver );
            stderrThread.setName(  makeName( "stderr handler" ) );
            stderrThread.start();
        }
	}

    /**
     * Starts the process, using data provided in the constructor. Creates a
     * wait thread, waiting for the process to finish, and then doing cleanup.
     */
	public void runAsync() throws IOException
	{
	    start();

	    // Start thread that is waiting for process termination
        waitThread = new Thread( new Runnable() {
            public void run() { waitFinished(); }
        } );
        waitThread.setName( makeName( "wait handler" ) );
        waitThread.start();
	}

	/**
     * Starts the process, using data provided in the constructor. Waits for the
     * process to finish and does cleanup. Returns exit code.
	 */
	public int run() throws IOException
	{
	    start();
	    waitFinished();
	    return process.exitValue();
	}

	public ProcessBuilder getBuilder() {
        return builder;
    }

    private void assertStarted() {
	    Asserts.state( started, "Process not started" );
	}

	public void destroy()
	{
        // This can happend if the process failed to start of has already
        // stopped.
	    if ( !started ) return;

	    // Close down the input threads
	    stderrReceiver.close();
	    stdoutReceiver.close();

		process.destroy();
		// When process stops the wait thread will get notified and do the cleanup.
	}

	public int waitFor()
	{
	    if ( !started ) return -1;
	    // TODO:
//	    assertStarted();

	    try {
            return process.waitFor();
        } catch ( InterruptedException exc ) {
            // What is this good for? Clear interrupted status?
            Thread.currentThread().interrupt();
            callback.handleError( exc );
            return -1;
        }
	}

	/**
	 * Invoked ither the calling thread or the wait thread, or by a client.
	 */
	private void waitFinished()
	{
	    assertStarted();

	    waitFor();

	    // Wait for in/out threads
	    try {
			stdinThread.join();
			if ( !redirectErrorStream ) {
				stderrThread.join();
			}
		} catch ( InterruptedException exc ) {
		    callback.handleError( exc );
		    Thread.currentThread().interrupt();
		}

	    stderrReceiver.close();
        stdoutReceiver.close();
        Util.close( outWriter );

        started = false;

        callback.signalTerminated( process.exitValue() );
	}

	public void write( String line ) throws IOException
	{
        assertStarted();
        outWriter.write( line );
	}

	public void write( char ch ) throws IOException
	{
	    if ( !started ) return;
        assertStarted();
	    outWriter.write( ch );
	}

    public void close() {
        Util.close( outWriter );
    }
}


class CommandTest
{
	public static void main( String[] args ) throws IOException, InterruptedException
	{
	    out.println( "Starting." );

//	    final ProcessHandler lsHandler = new ProcessHandler( ProcessCallback.STDOUT, "ls" );
//
//	    int code = lsHandler.run();

		final ProcessHandler
            grepHandler1 = new ProcessHandler( ProcessCallback.STDOUT, "grep", "...." ),
            grepHandler2 = new ProcessHandler( grepHandler1.pipeCallback(), "grep", "[si]" ),
    	    lsHandler = new ProcessHandler( grepHandler2.pipeCallback(), "ls" );

		grepHandler1.runAsync();
		grepHandler2.runAsync();
		int code = lsHandler.run();

		grepHandler2.waitFor();
		grepHandler1.waitFor();

		out.println( "Exit code: " + code );

		out.println( "Exiting." );
	}
}


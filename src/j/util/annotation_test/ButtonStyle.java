package j.util.annotation_test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To be used on a field that will be passed as a style parameter to a SWT widget, so that it
 * can be checked by an annotation processor.
 *
 * Experiment, not implemented.
 */
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface ButtonStyle {}


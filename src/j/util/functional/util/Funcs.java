package j.util.functional.util;

import j.util.functional.Fun1;

public class Funcs
{
	private Funcs() {}


	private static final Fun1<Object, Object> ID = new Fun1<Object, Object>() {
		@Override public Object run( Object arg ) {
			return arg;
		}
	};

	@SuppressWarnings( "unchecked" )
    public static <T_ARG, T_RET> Fun1<T_ARG, T_RET> id() {
		return (Fun1<T_ARG, T_RET>) ID;
	}

    //	comp( final Func1<? super T_ARG, ? extends T_MID> f1,
//	final Func1<?  super T_MID, ? extends T_RET> f2 )

	public static <T_ARG, T_MID, T_RET> Fun1<T_ARG, T_RET>
	comp( final Fun1<T_ARG, T_MID> f1, final Fun1<? super T_MID, T_RET> f2 )
	{
		return new Fun1<T_ARG, T_RET>() {
			public T_RET run( T_ARG arg ) {
				return f2.run( f1.run( arg ) );
			}
		};
	}

	public static <T_ARG, T_MID1, T_MID2, T_RET> Fun1<T_ARG, T_RET>
	comp(final Fun1<T_ARG, T_MID1> f1,
		 final Fun1<T_MID1, T_MID2> f2,
		 final Fun1<T_MID2, T_RET> f3 )
    {
		return new Fun1<T_ARG, T_RET>() {
			public T_RET run( T_ARG arg ) {
				return f3.run( f2.run( f1.run( arg ) ) );
			}
		};
	}


}

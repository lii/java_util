package j.util.functional.util;

import j.util.functional.Fun1;
import j.util.functional.Pred;
import j.util.functional.derived.And;
import j.util.util.Asserts;



public class Preds
{
    private Preds() {}  // Make constructing an instance impossible

	public static final Pred<Object> FALSE = 
		new Pred<Object>() {
			public Boolean run( Object c ) {
				return false;
			}
		};

	public static final Pred<Object> TRUE = 
		new Pred<Object>() {
			public Boolean run( Object c ) {
				return true;
			}
		};


	
	public static <A, B> Pred<B> compose(final Pred<A> outExpr, final Fun1<? super B, ? extends A> inExpr)
    {
    	return new Pred<B>() {
    		public Boolean run( B arg ) {
    			return outExpr.run( inExpr.run( arg ) ); 
    		}
    	};
    }
	
	public static Pred<Number> higherThen( final Number n ) {
		return new Pred<Number>() {
			public Boolean run( Number in ) {
				return in.doubleValue() > n.doubleValue();
			}
		};
	}
	
	public static Pred<Number> inRange( final Number low, final Number high ) {
		Asserts.arg( low.doubleValue() > high.doubleValue(), "high must be higher then low" );
		
		return new Pred<Number>() {
			public Boolean run( Number in ) {
				return low.doubleValue() <= in.doubleValue() && in.doubleValue() <= high.doubleValue();
			}
		};
	}

	
	public static <ARG_T> And<ARG_T> and( Pred<ARG_T> p1, Pred<ARG_T> p2 ) {
		return new And<ARG_T>( p1, p2 );
	}

	
	public static <T> Pred<T> toPred( final Fun1<T, Boolean> expr ) {
		return new Pred<T>() {
			public Boolean run( T arg ) {
				return expr.run( arg );
			}
		};
	}
	
	@SuppressWarnings("unchecked")
    public static <T> Pred<T> getFalse()
    {
    	return (Pred<T>) FALSE;
    }

	@SuppressWarnings("unchecked")
    public static <T> Pred<T> TRUE()
    {
    	return (Pred<T>) TRUE;
    }

	@SuppressWarnings("unchecked")
    public static <T> Pred<T> cast(Pred<? super T> p)
    {
    	return (Pred<T>) p;
    }
}

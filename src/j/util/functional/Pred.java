package j.util.functional;


public interface Pred<ARG_T> extends Fun1<ARG_T, Boolean>
{
    public Boolean run( ARG_T arg );
}

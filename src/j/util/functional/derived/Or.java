package j.util.functional.derived;

import j.util.functional.Pred;

public class Or<ARG_T> implements Pred<ARG_T>
{
	private Pred<ARG_T> p1, p2;

	public Or( Pred<ARG_T> p1, Pred<ARG_T> p2 ) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	@Override
	public Boolean run( ARG_T c )
	{
		return p1.run( c ) || p2.run( c );
	}
}

package j.util.functional.derived;

import j.util.functional.Pred;

public class RangePred<C extends Number> implements Pred<C>
{
	private C low, high;
	
	public RangePred( C low, C high ) {
		if ( low.doubleValue() > high.doubleValue() ) {
			throw new IllegalArgumentException( "low can not be higher than high" );
		}
	    this.low = low;
	    this.high = high;
    }

	@Override
    public Boolean run( C arg ) {
	    return low.doubleValue() <= arg.doubleValue() && arg.doubleValue() < high.doubleValue();
    }
}



package j.util.functional.derived;

import j.util.functional.Pred;


public class And<C> implements Pred<C>
{
	private Pred<C> p1, p2;

	public And( Pred<C> p1, Pred<C> p2 ) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	
	public static <C> And<C> make(	Pred<C> p1, Pred<C> p2 ) {
		return new And<C>( p1, p2 );
	}
	
	public Boolean run( C c )
	{
		return p1.run( c ) && p2.run( c );
	}
}

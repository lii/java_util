package j.util.functional;

public interface Pred2<T1, T2> extends Fun2<T1, T2, Boolean> 
{
	public Boolean run( T1 arg1, T2 arg2 );
}

package j.util.functional;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Listeners
{
    public static ActionListener makeAction(final Action1<ActionEvent> a) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a.run(e);
            }
        };
    }
    
    public static ActionListener makeAction(final Action0 a) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a.run();
            }
        };
    }

    
    
}



public class Test
{
    
}

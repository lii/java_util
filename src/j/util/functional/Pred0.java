package j.util.functional;


public interface Pred0<ARG_T>
{
    public boolean run();
}

package j.util.functional;


public interface Fun2<T1, T2, RET_T>
{
    RET_T run(T1 arg1, T2 arg2);
}

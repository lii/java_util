package j.util.functional;


public interface Fun1<ARG_T, RET_T>
{
    RET_T run(ARG_T arg);
}

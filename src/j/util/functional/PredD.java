package j.util.functional;

public interface PredD<T> extends Pred2<T, T>
{
	public Boolean run( T arg1, T arg2 );
}

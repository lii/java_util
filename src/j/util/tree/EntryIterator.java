package j.util.tree;

import j.util.lists.Coll;
import j.util.lists.Queue;
import j.util.lists.Stack;

import java.util.Iterator;
import java.util.List;


/**
 * Used to iterate over all the entris in a Tree<T>.
 *
 * This class is not exposed to clients, insted iterators that expose the
 * data values or the subtrees are exposed.
 *
 * Iterates either deepth or breadth first.
 */
@SuppressWarnings( "unchecked" )
public class EntryIterator<E> implements Iterator<TreeNode<E>>
{
	private Coll<Iterator<TreeNode<E>>> itrs;
	private TreeNode<E> next;

	public EntryIterator( TreeNode<E> node ) {
		this( node, true );
	}
	public EntryIterator( TreeNode<E> node, boolean deepthFirst )
	{
		if ( deepthFirst ) {
			itrs = new Stack<Iterator<TreeNode<E>>>();
		} else {
			itrs = new Queue<Iterator<TreeNode<E>>>();
		}

		if ( node != null ) {
			next = node;
			itrs.push( ((Iterable<TreeNode<E>>) node.getChildren()).iterator() );
		}
	}

	@Override
    public boolean hasNext()
	{
		return next != null;
    }

	@Override
    public TreeNode<E> next()
	{
		TreeNode<E> result = next;
		next = null;
		proceed();
		return result;
    }

	public static <T> T getLast( List<T> l ) {
		return l.get( l.size() );
	}

	private void proceed()
	{
		// Back up util an iterator has more elems
		while ( !itrs.isEmpty() ) {
			Iterator<TreeNode<E>> i = itrs.peek();
			if ( i.hasNext() ) {
				// Take next child on this level, return it and continue
				// with its childs.
				next = i.next();
				itrs.push( (Iterator<TreeNode<E>>) next.getChildren().iterator() );
				break;
			} else {
				// Out of childs on this level, backtrack to parent.
				itrs.pop();
			}
		}
	}

	@Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

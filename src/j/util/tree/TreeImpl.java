package j.util.tree;

import j.util.functional.Fun1;
import j.util.lists.StupidIteratorCollection;
import j.util.lists.Trees;
import j.util.util.Asserts;
import j.util.util.Util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;



/**
 * Tree structure
 *
 * Simply a tree with links to both parent and childs.
 */
public class TreeImpl<T> implements Collection<T>, Tree<T>
{
	private Comparator<Object> comp = NORMAL_COMPARATOR;

	private Entry<T> root;

	public TreeImpl( Comparator<Object> comp ) {
		this.comp = comp;
	}

	@SafeVarargs
	public TreeImpl( Comparator<Object> comp, T data, TreeImpl<T>... childs ) {
		this( data, childs );
		this.comp = comp;
	}

	@SafeVarargs
	public TreeImpl( T data, TreeImpl<T>... childs )
	{
	    this.root = new Entry<T>( data );
		for ( TreeImpl<T> e : childs ) {
			this.root.addChild( e.root );
			e.setParent( this.root );
		}
    }

	public TreeImpl() {
	}

	private TreeImpl( Entry<T> e, Comparator<Object> comp ) {
		this( (Node<T>) e, comp );
	}

	private TreeImpl( Node<T> e, Comparator<Object> comp ) {
		root = (Entry<T>) e;
		this.comp = comp;
	}

	@Override
	public T getData() {
		return root.getData();
	}

	public boolean contains( Object value )
	{
		for ( T v : this ) {
			if ( comp.compare( v, value ) == 0 ) return true;
		}
		return false;
	}

	@Override
	public boolean isSubtree( Tree<T> other )
	{
		TreeImpl<T> subtree = new TreeImpl<T>( comp );
		for ( TreeNode<T> v : entryIterable() ) {
			subtree.setRootNode( v );
			if ( other.equals( subtree ) ) return true;
		}
		return false;
	}

	@Override
	public Iterable<TreeImpl<T>> subtreeIterable() {
		return new StupidIteratorCollection<TreeImpl<T>>(
				Iterators.transform( new EntryIterator<T>( root ),
						new Function<TreeNode<T>, TreeImpl<T>>() {
							@Override public TreeImpl<T> apply( TreeNode<T> n ) {
								return new TreeImpl<T>( (Node<T>) n, comp );
							}
		} ) );
	}

	private Iterable<TreeNode<T>> entryIterable() {
		return new StupidIteratorCollection<TreeNode<T>>(
				new EntryIterator<T>( root ) );
	}

	@Override
	public void setData( T data ) {
		root = new Entry<T>( data );
	}

	public boolean add( T o ) {
		root.addChild( new Entry<T>( o ) );
		return true;
	}

	@Override @SuppressWarnings( "unchecked" )
    public void concat( Tree<? extends T> other )
	{
		// TODO: Ugly cast to TreeImpl.
		TreeImpl<T> treeImpl = (TreeImpl<T>) other;

		if ( isEmpty() ) {
			root = (Entry<T>) treeImpl.root;
			treeImpl.root.setParent( null );
		} else {
			root.addChild( (Entry<T>) treeImpl.root );
		}
	}

	@Override
	public Tree<T> findSubtree( Object value )
	{
		if ( value == null ) {
			return this;
		} else {
			Node<T> node = getNode( value );

			if ( node == null ) {
				return null;
			} else {
				return new TreeImpl<T>( node, comp );
			}
		}
	}

	public Tree<T> getChild( Object key ) {
		Entry<T> e = root.getChild( key );
		return e == null ? null : new TreeImpl<T>( e, comp );
	}

	@Override
	public T findData( Object key ) {
		Entry<T> e = getEntry( key, root );
		return e == null ? null : e.getData();
	}

	private void setRootNode( TreeNode<T> n ) {
		if ( n instanceof Entry<?> ) {
			root = (Entry<T>) n;
		} else {
			throw new IllegalArgumentException();
		}
	}

	private Entry<T> getEntry( Object value, Entry<T> e )
	{
		if ( e == null ) {
			return null;
		} else if ( comp.compare( e.getData(), value ) == 0 ) {
			// Entry found
			return e;
		} else {
			// Try with all the children
			for ( Entry<T> e2 : e.getChildren() ) {
				Entry<T> r = getEntry( value, e2 );
				if ( r != null ) return r;
			}
			return null;
		}
	}

	@Override
	public List<T> getParents()
	{
		if ( hasParent() ) {
			return parents( root.getParent() );
		} else {
			return Collections.emptyList();
		}
	}

	public List<T> toList() {
		List<T> l = new ArrayList<T>();
		for ( T e : this ) {
			l.add( e );
		}
		return l;
	}


	@Override
	public Tree<T> getParent()
	{
		Asserts.elem( hasParent(), "Trying to get parent when there is no parent" );
		return new TreeImpl<T>( root.getParent(), comp );
	}

	@Override
	public boolean hasParent()
	{
		return root.getParent() != null;
	}

	@Override
	public List<T> path( Object value ) {
		return path( value, root );
	}

	private List<T> path( Object value, Entry<T> e )
	{
		return parents( getEntry( value, root ) );
	}

	private static <T> List<T> parents( Entry<T> t )
	{
	    LinkedList<T> l = new LinkedList<T>();
		while ( t != null ) {
			l.addFirst( t.getData() );
			t = t.getParent();
		}
		return l;
    }

	private Node<T> getNode( Object value )
	{
		for ( TreeNode<T> e : entryIterable() ) {
			if ( comp.compare( ((Node<T>) e).getData(), value ) == 0 ) {
				return (Node<T>) e;
			}
		}
		return null;
	}

	public int size()
	{
		int s = 0;
		for ( @SuppressWarnings( "unused" ) T t : this ) {
			s++;
		}
		return s;
	}

	private void setParent( Entry<T> parent ) {
    	this.root.setParent( parent );
    }

	@SuppressWarnings( "unchecked" )
    @Override
    public boolean equals( Object o )
	{
		if ( !(o instanceof TreeImpl<?>) || o == null ) {
			return false;
		}

		Iterator<T> itr1 = this.iterator(),
			itr2 = ( (Tree<T>) o ).iterator();

		while ( itr1.hasNext() && itr2.hasNext() ) {
			if ( !Util.equals( itr1.next(), itr2.next() ) ) return false;
		}

		if ( itr1.hasNext() || itr2.hasNext() ) return false;

		return true;
    }

	public boolean isEmpty() {
		return root == null;
	}


	@Override
    public String toString()
	{
		return new StringBuilder()
				.append( '<' )
				.append( getClass().getSimpleName() )
				.append( ' ' )
				.append( Iterators.toString( iterator() ) )
				.append( '>' ).toString();
    }


	@SuppressWarnings( "unchecked" )
	@Override
    public Iterator<T> depthIterator() {
		return Iterators.transform( new EntryIterator<T>( root, true ),
				(Function<TreeNode<T>, T>) dataFunc );
    }

	@Override
	public Iterable<T> breadthIterable() {
		return new StupidIteratorCollection<T>( breadthIterator() );
	}

	@SuppressWarnings( "unchecked" )
	@Override
    public Iterator<T> breadthIterator() {
		return Iterators.transform( new EntryIterator<T>( root, false ),
				(Function<TreeNode<T>, T>) dataFunc );
    }



	@Override
	public Tree<T> copy()
	{
		Tree<T> result = new TreeImpl<T>( comp );

		for ( Tree<T> t : subtreeIterable() ) {

			if ( t.hasParent() ) {
				Tree<T> subtree = result.findSubtree( t.getParent().getData() );
				subtree.add( t.getData() );
			} else {
				result.setData( t.getData() );
			}
		}

		return result;
	}


	@Override
	public <T2> Tree<T2> transp( Fun1<T, T2> f )
	{
		if ( isEmpty() ) {
			// This isnt really the base case, it just handles the empty tree
			return Trees.empty( comp );
		} else {
			Tree<T2> newTree = new TreeImpl<T2>( f.run( getData() ) );

			// Empty list here is the base case
			for ( Tree<T> t : childs() ) {
				newTree.concat( t.transp( f ) );
			}

			return newTree;
		}
	}


	public Iterable<? extends Tree<T>> childs()
	{
		@SuppressWarnings( "unchecked" )
		Function<Entry<T>, TreeImpl<T>> treeFunc = (Function<Entry<T>, TreeImpl<T>>) TREE_FUNC;

		return  Iterables.transform( root.getChildren(), treeFunc );
	}


	@Override
    public Iterator<T> iterator() {
	    return depthIterator();
    }



	@Override
    public boolean addAll( Collection<? extends T> c )
	{
		boolean hasChanged = false;
		for ( T o : c ) {
			if ( add( o ) ) {
				hasChanged = true;
			}
		}
	    return hasChanged;
    }


	@Override
    public void clear() {
		root = null;
    }


	@Override
    public boolean containsAll( Collection<?> c ) {
		for ( Object o : c ) {
			if ( !contains( o ) ) return false;
		}
	    return true;
    }

	@Override
    public boolean remove( Object o ) {
		Entry<T> e = getEntry( o, root );
		if ( e != null ) {
			e.getParent().getChildren().remove( e );
			return true;
		} else {
			return false;
		}
    }

	@Override
    public boolean removeAll( Collection<?> c ) {
		boolean hasChanged = true;
		for ( Object o : c ) {
			if ( remove( o ) ) hasChanged = true;
		}
	    return hasChanged;
    }

	@Override
    public boolean retainAll( Collection<?> arg0 ) {
	    throw new UnsupportedOperationException();
    }

	@Override
    public Object[] toArray() {
	    Iterators.toArray( iterator(), Object.class );
	    return null;
    }

	@SuppressWarnings( "unchecked" )
    @Override
    public <R> R[] toArray( R[] arr )
	{
		int size = size();
		Class<?> cls = arr.getClass().getComponentType();

		if ( size > arr.length ) {
			arr = (R[]) Array.newInstance( cls, size );
		}

		int i = 0;
		for ( T e : this ) {
			if ( !cls.isInstance( e ) ) throw new ArrayStoreException();
			arr[ i++ ] = (R) e;
		}

	    return arr;
    }

	@Override
    public int hashCode() {
	    return super.hashCode();
    }

	private final static Comparator<Object> NORMAL_COMPARATOR =
		new Comparator<Object>() {
			@Override public int compare( Object arg1, Object arg2 ) {
		        return Util.equals( arg1, arg2 ) ? 0 : 1;
	        }
	};

	@Override
	@SuppressWarnings( "unchecked" )
	public void setComparator( Comparator<? super T> comp ) {
		this.comp = (Comparator<Object>) comp;
	}

	private static final Object dataFunc =
			new Function<Node<Object>, Object>() {
				@Override public Object apply( Node<Object> n ) {
					return n.getData();
				}
			};

		@SuppressWarnings( { "rawtypes", "unchecked" } )
		private static final Object TREE_FUNC =
			new Function<Node, TreeImpl>() {
						@Override public TreeImpl apply( final Node n ) {
							return new TreeImpl( n, (Comparator<Object>) null );
						}
					};


		public Function<Node<T>, TreeImpl<T>> treeFunc() {
			return new Function<Node<T>, TreeImpl<T>>() {
					@Override public TreeImpl<T> apply( Node<T> n ) {
						return new TreeImpl<T>( n, (Comparator<Object>) null );
					}
				};
		}

}




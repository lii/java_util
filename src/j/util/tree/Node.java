package j.util.tree;


public interface Node<T> extends TreeNode<T>
{
	public void setData( T data );
	public T getData();
}

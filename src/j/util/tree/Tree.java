package j.util.tree;

import j.util.functional.Fun1;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Interface for the Tree class
 */
public interface Tree<T> extends Collection<T>
{
	//
	// Minimal interface
	//

	/**
	 * Adds a child. A tree must have a root for this to work, you cannot add
	 * to an empty tree.
	 */
	public boolean add( T o );

	/**
	 * Gets the data associated with the root of this tree.
	 */
	public T getData();

	/**
	 * Sets the data associated with the root of this tree.
	 */
	public void setData( T data );

	/**
	 * Has this tree a parent? That is, is this tree subtree of another tree?
	 */
	public boolean hasParent();

	/**
	 * Gets the parent of this tree. Throws if there is to parent.
	 */
	public Tree<T> getParent();

	/**
	 * Does this tree represent the empty tree? An empty tree has to data and
	 * no children.
	 */
	public boolean isEmpty();

	/**
	 * Gets the childs of this tree. Empty list if there are none.
	 */
	public Iterable<? extends Tree<T>> childs();

	/**
	 * Adds other to this tree's children. If this tree is empty, sets data
	 * to otheres data. That means that this.concat( other ).equals( other ).
	 */
	public void concat( Tree<? extends T> other );

	//
	// Utility operations:
	//

	public Tree<T> getChild( Object key );
	public List<T> getParents();

	public T findData( Object key );
	public Tree<T> findSubtree( Object value );

	public boolean isSubtree( Tree<T> other );

	public List<T> path( Object value );

	public <NEW_T> Tree<NEW_T> transp( Fun1<T, NEW_T> f );

	public Iterator<T> depthIterator();
	public Iterable<T> breadthIterable();
	public Iterator<T> breadthIterator();
	public Iterable<? extends Tree<T>> subtreeIterable();

	public abstract void setComparator( Comparator<? super T> comp );
	public Tree<T> copy();
}


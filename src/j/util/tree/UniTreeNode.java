package j.util.tree;

import java.util.Collection;

public interface UniTreeNode<T>  {
	public Collection<? extends UniTreeNode<T>> getChildren();
}

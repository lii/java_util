package j.util.tree;

import java.util.Collection;


public interface TreeNode<T> extends UniTreeNode<T> {
	public TreeNode<T> getParent();
	@Override
	public Collection<? extends TreeNode<T>> getChildren();
}

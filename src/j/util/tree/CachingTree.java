package j.util.tree;

import j.util.functional.Fun1;
import j.util.util.NotImplementedException;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Function;

public class CachingTree<T> implements Tree<T> {
	private TreeImpl<T> tree;

	public void buildCache() {

	}





	public void concat( Tree<? extends T> other ) {
		tree.concat( other );
	}





	@Override public Tree<T> getChild( Object key ) {
		// TODO Auto-generated method stub
		throw new NotImplementedException();
	}





	public T getData() {
		return tree.getData();
	}

	public boolean contains( Object value ) {
		return tree.contains( value );
	}

	public boolean isSubtree( Tree<T> other ) {
		return tree.isSubtree( other );
	}

	public Iterable<TreeImpl<T>> subtreeIterable() {
		return tree.subtreeIterable();
	}

	public void setData( T data ) {
		tree.setData( data );
	}

	public boolean add( T o ) {
		return tree.add( o );
	}

	public Tree<T> findSubtree( Object value ) {
		return tree.findSubtree( value );
	}

	public T findData( Object key ) {
		return tree.findData( key );
	}

	public List<T> getParents() {
		return tree.getParents();
	}

	public List<T> toList() {
		return tree.toList();
	}

	public Tree<T> getParent() {
		return tree.getParent();
	}

	public boolean hasParent() {
		return tree.hasParent();
	}

	public List<T> path( Object value ) {
		return tree.path( value );
	}

	public int size() {
		return tree.size();
	}

	public boolean equals( Object o ) {
		return tree.equals( o );
	}

	public boolean isEmpty() {
		return tree.isEmpty();
	}

	public String toString() {
		return tree.toString();
	}

	public Iterator<T> depthIterator() {
		return tree.depthIterator();
	}

	public Iterable<T> breadthIterable() {
		return tree.breadthIterable();
	}

	public Iterator<T> breadthIterator() {
		return tree.breadthIterator();
	}

	public Tree<T> copy() {
		return tree.copy();
	}

	public <T2> Tree<T2> transp( Fun1<T, T2> f ) {
		return tree.transp( f );
	}

	public Iterable<? extends Tree<T>> childs() {
		return tree.childs();
	}

	public Iterator<T> iterator() {
		return tree.iterator();
	}

	public Function<Node<T>, TreeImpl<T>> treeFunc() {
		return tree.treeFunc();
	}

	public boolean addAll( Collection<? extends T> c ) {
		return tree.addAll( c );
	}

	public void clear() {
		tree.clear();
	}

	public boolean containsAll( Collection<?> c ) {
		return tree.containsAll( c );
	}

	public boolean remove( Object o ) {
		return tree.remove( o );
	}

	public boolean removeAll( Collection<?> c ) {
		return tree.removeAll( c );
	}

	public boolean retainAll( Collection<?> arg0 ) {
		return tree.retainAll( arg0 );
	}

	public Object[] toArray() {
		return tree.toArray();
	}

	public <R> R[] toArray( R[] arr ) {
		return tree.toArray( arr );
	}

	public int hashCode() {
		return tree.hashCode();
	}

	public void setComparator( Comparator<? super T> comp ) {
		tree.setComparator( comp );
	}

}

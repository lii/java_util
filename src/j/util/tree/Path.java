package j.util.tree;

import j.util.lists.Li;


/**
 * Represents the path throw a Tree a node, by nameing the interconnecting nodes.
 * 
 * NOT USED.
 */
public class Path
{
	@SuppressWarnings( "unused" ) 
	private Li<Object> path;
}

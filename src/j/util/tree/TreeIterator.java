package j.util.tree;

import static java.lang.System.out;
import j.util.util.NotImplementedException;

import java.util.Iterator;

import com.google.common.base.Strings;

class Iter<T> {

	public boolean movedDown() {
		return false;
	}

	public boolean movedUp() {
		return false;
	}

	public T getData() {
		return null;
	}
}

public class TreeIterator<T> implements Iterator<Iter<T>>{

	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public Iter<T> next() {
		return null;
	}

	@Override
	public void remove() {
		throw new NotImplementedException();
	}

}




class Main {

	@SuppressWarnings( "unchecked" )
	public static void main( String[] args )
	{
		Tree<String> tree = new TreeImpl<String>();

		int indent = 0;

		for ( Iter<String> iter : (Iterable<Iter<String>>) (Object) tree ) {

			if ( iter.movedDown() ) {
				indent += 4;
			} else if ( iter.movedUp() ) {
				indent -= 4;
			} else {
				out.println( Strings.repeat( " ", indent ) + iter.getData() );
			}
		}
	}



}





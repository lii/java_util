package j.util.tree;

import j.util.util.Util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Entry<T> implements Node<T>
{
	private T data;
	private Entry<T> parent;
	private List<Entry<T>> childs = Collections.emptyList();

	public Entry( T data ) {
	    this.setData( data );
    }

	public Entry( T data, List<Entry<T>> childs ) {
	    this.setData( data );
	    this.setChilds( childs );
    }

	public void setChilds( List<Entry<T>> childs ) {
	    this.childs = childs;
    }

	public List<Entry<T>> getChildren() {
	    return childs;
    }

	public Entry<T> getChild( Object key ) {
		for ( Entry<T> e : childs ) {
			if ( Util.equals( e.getData(), key ) ) return e;
		}
		return null;
	}

	public void setParent( Entry<T> parent ) {
	    this.parent = parent;
    }

	public Entry<T> getParent() {
	    return parent;
    }

	public void setData( T data ) {
	    this.data = data;
    }

	public T getData() {
	    return data;
    }

	private void verifyChilds() {
		if ( childs == Collections.EMPTY_LIST ) {
			childs = new LinkedList<Entry<T>>();
		}
	}

	public boolean addChild( Entry<T> e ) {
		verifyChilds();
		e.parent = this;
	    return childs.add( e );
    }

	@Override
    public String toString() {
	    return Util.simpleToString( this, data );
    }
}

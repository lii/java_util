package j.util.streams;

import static java.util.stream.Collectors.toList;
import j.util.util.Pair;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamUtil {
    private StreamUtil() {}
    
    
    /**
     * Performs a map in place in the list l. After the operation l will contain elements that
     * potentially are of the wrong type.
     */
    @SuppressWarnings("unchecked")
    public static <O, R> ArrayList<R> transform(ArrayList<O> l, Function<O, R> f) {
        ArrayList<Object> rl = (ArrayList<Object>) l;
        
        for (int i = 0; i < l.size(); i++) {
            rl.set(i, f.apply(l.get(i)));
        }
        
        return (ArrayList<R>) rl;
    }
    
    public static <A, R> Function<A, R> runtimizing(ThrowingFunction<A, R> f) {
        return a -> {
            try {
                return f.apply(a); 
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("runtimize", exc);
            }
        };
    }
    
    public static Runnable runtimizing(ThrowingRunnable r) {
        return () -> {
            try {
                r.run(); 
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("runtimize", exc);
            }
        };
    }

    public static <T> Consumer<T> runtimizing(ThrowingConsumer<T> c) {
        return o -> {
            try {
                c.accept(o);
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("runtimize", exc);
            }
        };
    }


    public static <T> T runtimize(Callable<T> a) {
        try {
            return a.call();
        } catch (RuntimeException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new RuntimeException("Wrapped in runtimize", exc);
        }
    }
    
    public static void runtimize(ThrowingRunnable r) {
        try {
            r.run();
        } catch (RuntimeException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new RuntimeException("runtimize", exc);
        }
    }
    
    public static <A, B> Function<A, B> consumerToFunction(Consumer<A> c) {
        return o -> {
            c.accept(o);
            return null;
        };
    }

    public static <A> Function<?, A> supplierToFunction(Supplier<A> c) {
        return o -> c.get();
    }

    
    public static <A extends AutoCloseable, B> B applyAndClose(Callable<A> sup, Function<? super A, B> f) {
        try (A res = sup.call()) {
            return f.apply(res);
        } catch (RuntimeException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new RuntimeException("Wrapped in applyAndClose", exc);
        }
    }
    
    public static <A extends AutoCloseable> void acceptAndClose(Callable<A> sup, Consumer<A> f) {
        applyAndClose(sup, consumerToFunction(f));
    }
    
    public static <A> CloseableWrapper<A> toClosable(A o, Consumer<A> closer) {
        return new CloseableWrapper<>(o, closer);
    }
    
    public static <A, B> Stream<Pair<A, B>> zip(Stream<A> as, Stream<B> bs) {
        return StreamSupport.stream(
            new ZipSpliterator<>(as.spliterator(), bs.spliterator(), Pair<A, B>::new, false, true), false);
    }
    
    public static <A, B, R> Stream<R> zipWith(Stream<A> as, Stream<B> bs, BiFunction<? super A, ? super B, R> zipper) {
        return StreamSupport.stream(
            new ZipSpliterator<>(as.spliterator(), bs.spliterator(), zipper, true, false), false);
    }
    
    /**
     * Returns a parallel stream with the given batch size.
     */
    public static <T> Stream<T> toFixedBatchStream(Stream<T> in, int batchSize) {
        return StreamSupport.stream(new FixedBatchSpliteratorWrapper<>(in.spliterator(), batchSize), true);
    }


    
    public static class CloseableWrapper<T> implements AutoCloseable {
        private final T o;
        private final Consumer<T> closer;
        
        public CloseableWrapper(T o, Consumer<T> closer) {
            this.o = o;
            this.closer = closer;
        }

        public T get() {
            return o;
        }
        
        public void close() {
            closer.accept(o);
        }
    }
    
    public static void main(String[] args) {
        System.out.println(zip(Stream.of(1, 2, 3, 4), Stream.of("a", "b", "c")).collect(toList()));
        System.out.println(zip(Stream.empty(), Stream.of("a", "b", "c")).collect(toList()));
    }

    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Debug utilities 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    private static void debugPrint(String msg) {
        if (doDebug) System.out.println(msg);
    }
    
    public static boolean doDebug = true;
    
    private static Map<String, Integer> printedYears = new ConcurrentHashMap<>();
    
    public static <A> BinaryOperator<A> wrapThreadPrint(String msg, int skip, BiFunction<A, A, A> f) {
        return (acc, e) -> wrapThreadPrint(msg, skip,
            (Function<A, A>) a -> f.apply(acc, a)).apply(e);
    }
    
    public static <A, R> Function<A, R> wrapThreadPrint(String msg, int skip, Function<A, R> f) {
        return o -> {
            int newCount;
            
            if (skip == 0) {
                newCount = 0;
            } else if (skip > 0) {
                newCount = printedYears.compute(msg,
                    (str, i) -> i == null || i == 0 ? skip : i - 1);
            } else {
                newCount = -1;
            }
            
            if (newCount == 0) {
                String oStr = o.toString();
                debugPrint(msg + " " + oStr.substring(
                        oStr.toString().length() - Math.min(oStr.length(), 20))
                    + ": " + Thread.currentThread().getName());
            }
            return f.apply(o);
        };
    }

}

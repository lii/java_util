package j.util.streams;

@FunctionalInterface
public interface ThrowingConsumer<T> {
    void accept(T o) throws Exception;
}

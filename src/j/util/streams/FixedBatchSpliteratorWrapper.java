package j.util.streams;

import j.util.util.Box;

import java.util.Comparator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

/**
 * Spliterator to iterate streams is parallel with a fixed batch size. Uses because the
 * default spliterator for streams of unknown sizes use a 1024 element batch size and
 * we want smaller chunks for the year directories. 
 * 
 * Obtained from Stack Overflow answer:
 * http://stackoverflow.com/questions/22569040/readerlines-parallelizes-badly-due-to-nonconfigurable-batch-size-policy-in-it/22569900#22569900
 * 
 * @author Marko Topolnik
 */
public class FixedBatchSpliteratorWrapper<T> implements Spliterator<T> {
    private final Spliterator<T> spliterator;
    private final int batchSize;
    private final int characteristics;
    private long estSize;
    
    public FixedBatchSpliteratorWrapper(Spliterator<T> wrapped, int batchSize, long estSize) {
        int c = wrapped.characteristics();
        this.characteristics = (c & SIZED) != 0 ? c | SUBSIZED : c;
        this.spliterator = wrapped;
        this.batchSize = batchSize;
        this.estSize = estSize;
    }
    
    public FixedBatchSpliteratorWrapper(Spliterator<T> toWrap, int batchSize) {
        this(toWrap, batchSize, toWrap.estimateSize());
    }
    
    @Override
    public Spliterator<T> trySplit() {
        Box<T> holder = new Box<>(null);
        
        if (!spliterator.tryAdvance(holder)) return null;
        
        Object[] arr = new Object[batchSize];
        int j = 0;
        
        do {
            arr[j] = holder.value;
        } while (++j < batchSize && spliterator.tryAdvance(holder));
        
        if (estSize != Long.MAX_VALUE) estSize -= j;
        
        return Spliterators.spliterator(arr, 0, j, characteristics());
    }
    
    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        return spliterator.tryAdvance(action);
    }
    
    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        spliterator.forEachRemaining(action);
    }
    
    @Override
    public Comparator<? super T> getComparator() {
        if (hasCharacteristics(SORTED)) {
            return spliterator.getComparator();
        } else {
            throw new IllegalStateException();
        }
    }
    
    @Override
    public long estimateSize() {
        return estSize;
    }
    
    @Override
    public int characteristics() {
        return characteristics;
    }
}
package j.util.streams;

@FunctionalInterface
public interface ThrowingFunction<A, R> {
    R apply(A a) throws Exception;
}
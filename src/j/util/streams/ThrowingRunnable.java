package j.util.streams;

@FunctionalInterface
public interface ThrowingRunnable {
    void run() throws Exception;
}
package j.util.streams;

import j.util.util.Box;

import java.util.Objects;
import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;

class ZipSpliterator<A, B, R> implements Spliterator<R> {
    private final Spliterator<A> splitA;
    private final Spliterator<B> splitB;
    private final Box<Object> box = new Box<>(null);
    private final int characteristics;
    private final BiFunction<? super A, ? super B, R> zipper;
    
    public ZipSpliterator(Spliterator<A> splitA, Spliterator<B> splitB, BiFunction<? super A, ? super B, R> zipper, boolean zippedNullable, boolean zippedOrdered) {
        this.splitA = splitA;
        this.splitB = splitB;
        this.zipper = zipper;
        this.characteristics = calcCharacteristics(zippedNullable, zippedOrdered);
    }

    private int calcCharacteristics(boolean zippedNullable, boolean zippedOrdered) {
        int c =
            // Set if both
            splitA.characteristics() & splitB.characteristics()
                & (IMMUTABLE | CONCURRENT | SUBSIZED | SIZED | ORDERED);
        
        // Clear ORDERED if zippedOrdered is false
        if (!zippedOrdered) c &= ~ORDERED;

        // Set if either
        c |= (splitA.characteristics() | splitB.characteristics()) & DISTINCT;
        
        c &= ~SORTED; // Clear

        // Set NONNULL to same as zippedNullable
        return zippedNullable ? c & ~Spliterator.NONNULL : c | Spliterator.NONNULL;
        
    }    

    @SuppressWarnings("unchecked")
    @Override
    public boolean tryAdvance(Consumer<? super R> action) {
        Objects.requireNonNull(action, "tryAdvance action may not be null");
        
        // Using non-shortcut because both should attempt advancement even if one fails
        if (splitA.tryAdvance(box) & splitB.tryAdvance(box)) {
            action.accept(zipper.apply((A) box.value, (B) box.value));
            return true;
        } else {
            return false;
        }
    }

    /*
     * Never splits. Use wrapping spliterator to get sensible splitting behaviour.
     */
    @Override
    public Spliterator<R> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return Long.min(splitA.estimateSize(), splitB.estimateSize());
    }

    @Override
    public int characteristics() {
        return characteristics;
    }
    
}
package j.util.util;

import java.util.function.Consumer;

public class Box<T> implements Consumer<T> {
	public T value;

	public Box( T value ) {
		this.value = value;
	}
	
	public void set(T value) {
	    this.value = value;
	}
	
	public T get() {
	    return value;
	}

    public void accept(T value) {
        set(value);
    }

}

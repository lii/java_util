package j.util.util;

public interface Disposable
{
	public void dispose();
}

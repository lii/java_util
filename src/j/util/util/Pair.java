package j.util.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class Pair<A, B> {
    public final A first;
    public final B second;
    
    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }
    
    public <N> Pair<N, B> updateFirst(N a) {
        return new Pair<>(a, getSecond());
    }
    
    public <N> Pair<A, N> updateSecond(N b) {
        return new Pair<>(getFirst(), b);
    }
    
    public static <A, B> Pair<A, B> make(A a, B b) {
        return new Pair<A, B>(a, b);
    }
    
    public A getFirst() {
        return first;
    }
    
    public B getSecond() {
        return second;
    }
    
    public Object get(int index) {
        if (index == 0) return first;
        else if (index == 1) return second;
        else throw new IndexOutOfBoundsException();
    }
    
    public static <TC, T1 extends TC, T2 extends TC> List<TC> toList(Pair<T1, T2> p) {
        List<TC> l = new ArrayList<>(2);
        l.add(p.first);
        l.add(p.second);
        return l;
    }

    
    public static <A, B> Comparator<Pair<A, B>> lexicalComparator(Comparator<? super A> compA, Comparator<? super B> compB) {
//        return Comparator.comparing(Pair<A, B>::getFirst, compA)
//            .thenComparing(Pair::getSecond, compB);
//        return (p1, p2) -> {
//            int r = compA.compare(p1.first, p2.first);
//            return r != 0 ? r : compB.compare(p1.second, p2.second);
//        };
//         Code for handling null in a sensible way. This turned out to be to tricky to
//         be worth the effort.
         return (p1, p2) -> {
             if (p1 == null && p2 == null) return 0;
             if (p1 == null) return -1;
             if (p2 == null) return 1;
             int r = compA.compare(p1.first, p2.first);
             if (r != 0) return r;
             return compB.compare(p1.second, p2.second);
         };
    }
    
    @Override
    public String toString() {
        return "<Pair " + first + ", " + second + ">";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( (first == null) ? 0 : first.hashCode());
        result = prime * result + ( (second == null) ? 0 : second.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Pair<?, ?> other = (Pair<?, ?>) obj;
        if (first == null) {
            if (other.first != null) {
                return false;
            }
        } else if (!first.equals(other.first)) {
            return false;
        }
        if (second == null) {
            if (other.second != null) {
                return false;
            }
        } else if (!second.equals(other.second)) {
            return false;
        }
        return true;
    }

}


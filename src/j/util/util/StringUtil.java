package j.util.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public final class StringUtil {
	private StringUtil() {}

	public static String join( Iterable<?> itr, String s )
	{
		StringBuilder b = new StringBuilder();

		boolean isFirst = true;

		for ( Object o : itr ) {
			if ( !isFirst ) b.append( s );
			isFirst = false;

			b.append( o.toString() );
		}

		return b.toString();
	}


	public static List<String> splitWithQuotes( String s )
	{
		List<String> result = new ArrayList<String>();

		StringBuilder b = new StringBuilder();
		char ch, quote;

		for ( int i = 0; i < s.length(); i++ ) {

			ch = s.charAt( i );

			if ( ch == '\\' ) {

				i++;
				if ( i >= s.length() ) throw new IllegalArgumentException( "'\\' as end of string" );
				b.append( s.charAt( i ) );

			} else if ( Character.isWhitespace( ch ) ) {

				if ( b.length() > 0 ) {
					result.add( b.toString() );
					b.setLength( 0 );
				}

			} else if ( ch == '"' || ch == '\'' ) {

				quote = ch;

				while ( true ) {

					i++;
					if ( i >= s.length() ) throw new IllegalArgumentException( "Unbalanced quotes" );
					ch = s.charAt( i );

					if ( ch == quote ) {
						if ( s.length() > i + 1 && b.length() == 0
								&& Character.isWhitespace( s.charAt( i + 1 ) ) )
						{
							result.add( "" );
						}
						break;
					}

					if ( ch == '\\' ) {
						i++;
						if ( i >= s.length() ) throw new IllegalArgumentException( "'\\' as end of string" );
						ch = s.charAt( i );
					}

					b.append( ch );
				}

			} else {

				b.append( ch );

			}

		}

		if ( b.length() > 0 ) result.add( b.toString() );

		return result;
	}





	/** Holds the number 1-19, dual purpose for special cases (teens) **/
	private static final String[] UNITS = { "one", "two", "three", "four",
			"five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
			"thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
			"eighteen", "nineteen" };

	/** Holds the tens places **/
	private static final String[] TENS = { "ten", "twenty", "thirty", "forty",
			"fifty", "sixty", "seventy", "eighty", "ninty" };

	/** Covers max value of Long **/
	private static final String[] THOUSANDS = { "", "thousand", "million",
			"billion", "trillion", "quadrillion", "quintillion", "sextillion" };

	/**
	 * Represents a number in words (seven hundred thirty four, two hundred and
	 * seven, etc...)
	 *
	 * The largest number this will accept is
	 *
	 * <pre>
	 * 999,999,999,999,999,999,999
	 * </pre>
	 *
	 * but that's okay becasue the largest value of Long is
	 *
	 * <pre>
	 * 9,223,372,036,854,775,807
	 * </pre>
	 *
	 * . The smallest number is
	 *
	 * <pre>
	 * -9,223,372,036,854,775,807
	 * </pre>
	 *
	 * (Long.MIN_VALUE +1) due to a limitation of Java.
	 *
	 * @param number
	 *            between Long.MIN_VALUE and Long.MAX_VALUE
	 * @return the number writen in words
	 */
	public static String numberInWords( long number )
	{
		StringBuilder sb = new StringBuilder();
		// Zero is an easy one, but not technically a number :P
		if ( number == 0 ) {
			return "zero";
		}
		// Negative numbers are easy too
		if ( number < 0 ) {
			sb.append( "minus " );
			number = Math.abs( number );
		}

		// Log keeps track of which Thousands place we're in
		long log = 1000000000000000000L,
			sub = number;
		int i = 7;

		do {
			// This is the 1-999 subset of the current thousand's place
			sub = number / log;
			// Cut down number for the next loop
			number = number % log;
			// Cut down log for the next loop
			log = log / 1000;
			i--; // tracks the big number place
			if ( sub != 0 ) {
				// If this thousandths place is not 0 (that's okay, just skip)
				// tack it on
				hundredsInWords( sb, (int) sub );
				sb.append( " " );
				sb.append( THOUSANDS[ i ] );
				if ( number != 0 ) {
					sb.append( " " );
				}
			}
		} while ( number != 0 );

		return sb.toString();
	}

	/**
	 * Converts a number into hundreds.
	 *
	 * The basic unit of the American numbering system is "hundreds". Thus 1,024
	 * = (one thousand) twenty four 1,048,576 = (one million) (fourty eight
	 * thousand) (five hundred seventy six) so there's no need to break it down
	 * further than that.
	 *
	 * @param n
	 *            between 1 and 999
	 * @return
	 */
	private static void hundredsInWords( StringBuilder sb, int n )
	{
		if ( n < 1 || n > 999 ) {
			throw new AssertionError( n ); // Use assersion errors in private
											// methods only!
		}
//		StringBuilder sb = new StringBuilder();

		// Handle the "hundred", with a special provision for x01-x09
		if ( n > 99 ) {
			sb.append( UNITS[ ( n / 100 ) - 1 ] );
			sb.append( " hundred" );
			n = n % 100;
			if ( n != 0 ) {
				sb.append( " " );
			}
			if ( n < 10 ) {
				sb.append( "and " );
			}
		}

		// Handle the special cases and the tens place at the same time.
		if ( n > 19 ) {
			sb.append( TENS[ ( n / 10 ) - 1 ] );
			n = n % 10;
			if ( n != 0 ) {
				sb.append( " " );
			}
		}

		// This is the ones place
		if ( n > 0 ) {
			sb.append( UNITS[ n - 1 ] );
		}
	}

	public static String makeFirstCap( String s ) {
		return Character.toUpperCase( s.charAt( 0 ) ) + s.substring( 1 );
	}


	public static String collToString( Iterable<?> coll )
	{
		StringBuilder builder = new StringBuilder();

		boolean first = true;

		for ( Object o : coll ) {
			if ( !first ) builder.append( "|" );
			first = false;
			builder.append( escapeReplace( o.toString(), '\\', 'S', '|', 'P' ) );
		}

		return builder.toString();
	}


	public static String escape( String s, String esq, String esqRepl,
			String tok, String tokRepl )
	{
		return s.replace( esq, esq + esqRepl )
				.replace(  tok, esq + tokRepl );
	}

	public static List<String> stringToColl( String s )
	{
		StringTokenizer tok = new StringTokenizer( s, "|" );

		ArrayList<String> result = new ArrayList<String>( 5 );

		while ( tok.hasMoreTokens() ) {
			result.add( unesqapeReplace( tok.nextToken(), '\\', 'S', '|', 'P' ) );
		}

		return result;
	}


	/**
	 * Unescapes a string that has been escaped with escapeReplace.
	 */
	public static String unesqapeReplace( String s, char esq, char esqRepl,
			char tok, char tokRepl )
	{
		StringBuilder builder = new StringBuilder( s.length() );

		for ( int i = 0; i < s.length(); i++ ) {

			char ch = s.charAt( i );

			if ( ch == esq ) {

				char next = s.charAt( ++i );

				if ( next == tokRepl ) {
					builder.append( tok );
				} else if ( next == esqRepl ) {
					builder.append( esq );
				} else {
					throw new IllegalArgumentException( "String and escape chars does not match" );
				}

			} else {
				builder.append( ch );
			}
		}

		return builder.toString();
	}

	/**
	 * Returns a version of s where tok has been replaced with esc + tokReplace,
	 * and esc has been replaced with escRepl + escRepl
	 */
	public static String escapeReplace( String s, char esc, char escRepl,
			char tok, char tokRepl )
	{
		StringBuilder builder = new StringBuilder( s.length() + 3 );

		for ( int i = 0; i < s.length(); i++ ) {
			char ch = s.charAt( i );

			if ( ch == esc ) {
				builder.append( esc ).append( escRepl );
			} else if ( ch == tok ) {
				builder.append( esc ).append( tokRepl );
			} else {
				builder.append( ch );
			}
		}

		return builder.toString();
	}



}

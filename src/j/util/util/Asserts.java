package j.util.util;

import java.util.NoSuchElementException;

public final class Asserts {
	private Asserts() {}

	public static void notNull( Object o ) {
		Asserts.notNull( o, "Null!" );
	}

	public static void notNull( Object o, String msg ) {
		if ( o == null ) {
			throw new NullPointerException( msg );
		}
	}

	public static <T> void argEqual( T arg, T exp, String message ) {
		if ( !arg.equals( exp ) ) {
			throw new IllegalArgumentException( message + " Was: " + arg + " Expected: " + exp  );
		}
	}

	public static void arg( boolean expr, String message )
	{
		if ( !expr ) {
			throw new IllegalArgumentException( message );
		}
	}

	public static void state( boolean expr, String msg ) {
		if ( !expr ) {
			throw new IllegalStateException( msg );
		}
	}

	public static void args( boolean expr1, String message1, boolean expr2, String message2 )
	{
		arg( expr1, message1 );
		arg( expr2, message2 );
	}

	public static <T extends Comparable<T>> void argRange( T arg, T l1, T l2, String name )
	{
		if ( !Util.inRange( arg, l1, l2 ) ) {
			throw new IllegalArgumentException( "Argument " + name + " must be between " + l1 + " and " + l2 );
		}
	}

	public static void verifyArgInRange( double arg, double l1, double l2, String name )
	{
		if ( !Util.inRange( arg, l1, l2 ) ) {
			throw new IllegalArgumentException( "Argument " + name + " must be between " + l1 + " and " + l2 );
		}
	}

	public static void elem( boolean expr, String message ) {
		if ( !expr ) {
			throw new NoSuchElementException( message );
		}
	}



}

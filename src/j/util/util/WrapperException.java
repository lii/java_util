package j.util.util;

public class WrapperException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

	public WrapperException( String arg0, Throwable arg1 ) {
	    super( arg0, arg1 );
    }

	public WrapperException( Throwable arg0 ) {
	    super( arg0 );
    }
}

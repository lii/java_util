package j.util.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<E> implements Iterator<E>
{
	private E[] array;
	private int index = 0,
			max;


	public ArrayIterator( E[] array ) {
	    this.array = array;
	    this.max = array.length;
    }


	public ArrayIterator( E[] array, int max ) {
		this.array = array;
		this.max = max;
	}



	@Override
    public boolean hasNext() {
	    return index < max;
    }

	@Override
    public E next()
	{
		if ( hasNext() ) {
			int i = index;
			index++;
			return array[i];
		} else {
			throw new NoSuchElementException();
		}
    }

	@Override
    public void remove() {
	    throw new UnsupportedOperationException();
    }

}

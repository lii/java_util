package j.util.util;

import j.util.functional.ActionTrow1;
import j.util.functional.Fun1;
import j.util.lists.Li;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



public class ReflUtil
{
	private ReflUtil() {}

	public static <T> void copy(T target, T source)
	{
		Asserts.arg( target.getClass() != source.getClass(), "Classes do not match" );

		try {
			Class<?> c = source.getClass();

			for (Field f : getAllFields( c ) ) {
				if ( !f.isAnnotationPresent( Nocopy.class ) ) {
					f.setAccessible( false );
					f.set( target, f.get( source ) );
				}
			}
		} catch ( IllegalArgumentException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		} catch ( IllegalAccessException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}
	}
	
	public static boolean isBoxed(Class<?> c) {
            return Byte.class.equals(c) 
                || Short.class.equals(c)
                || Integer.class.equals(c)
                || Long.class.equals(c)
                || Float.class.equals(c)
                || Double.class.equals(c)
                || Character.class.equals(c)
                || Boolean.class.equals(c);
	}


	public static void forEachField( Class<?> cls, ActionTrow1<Field> fn ) {
		try {
		Field[] fs = cls.getFields();

		for ( Field f : fs ) {
				fn.run( f );
		}

		} catch ( Exception exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}
	}

	public static List<Field> getAllFields( Class<?> cls )
	{
		List<Field> fields = new LinkedList<Field>();

		for ( Class<?> c : hierarchyIterable( cls ) ) {
			for ( Field f : c.getDeclaredFields() ) {
				if ( !Modifier.isStatic( f.getModifiers() ) ) {
					fields.add( f );
				}
			}
		}

		return fields;
	}


	public static <T> List<T> getAllInHierarchy( Class<?> cls, Fun1<Class<?>, T> getter )
	{
		List<T> members = new LinkedList<T>();

		while ( cls != null ) {
			members.add( getter.run( cls ) );
			cls = cls.getSuperclass();
		}

		return members;
	}

	public static <T> Iterable<T> makeIterable( final Iterator<T> itr ) {
		return new Iterable<T>() {
			public Iterator<T> iterator() {
				return itr;
			}
		};
	}

	public static Iterable<Class<?>> hierarchyIterable( final Class<?> cls ) {
		return makeIterable( new HierarchyIterator( cls ) );
	}


	public static Value getValuesUnchecked( Object o, Li<Object> seen )
		throws IllegalArgumentException, IllegalAccessException
	{
		Value v = null;

		if ( o == null ) {
			v = Value.makePrim( o );
		} else if ( seen.contains( o ) ) {
			v = Value.makePrim( "Circle: " + o.getClass().getSimpleName() );
		} else if ( o.getClass().isArray() ) {
			v = arrayToValue( o, seen );
		} else {
			v = objToValue( o, seen );
		}
		return v;
	}

	private static Value arrayToValue( Object o, Li<Object> seen )
		throws IllegalAccessException
	{
		int length = Array.getLength( o );
		List<Value> childs = new LinkedList<Value>();
		for ( int i = 0; i < length; i++ ) {
			childs.add( getValuesUnchecked( Array.get( o, i ), seen ) );
		}
		return Value.makeArray( childs, o );
	}

	private static Value objToValue( Object o, Li<Object> seen )
		throws IllegalArgumentException, IllegalAccessException
	{
		List<Field> fields = getAllFields( o.getClass() );

		if ( fields.size() == 0 ) {
			return Value.makePrim( o );
		} else {
			List<Entry<String, Value>> childs = new LinkedList<Entry<String, Value>>();
			for ( Field f : fields ) {
				f.setAccessible( true );
				Value value;
				if ( f.getType().isPrimitive() ) {
					value = Value.makePrim( f.get( o ) );
				} else {
					value = getValuesUnchecked( f.get( o ), seen.cons( o ) );
				}
				childs.add( Entry.make( f.getName(), value ) );
			}
			return Value.makeComp( childs, o );
		}
	}

	public static class Value
	{
		private boolean isPrimitive = false,
			isArray = false;

		public Object value;
		public List<Entry<String, Value>> childs;
		public Class<?> type;
		public Object obj;
		public List<Value> arrChilds;

		private Value() {}

		public boolean isPrimitive() {
			return isPrimitive;
		}

		public boolean isArray() {
        	return isArray;
        }

		public static Value makePrim( Object o ) {
			Value v = new Value();
			v.isPrimitive = true;
			v.value = o;
			v.obj = o;
			return v;
		}

		public static Value makeComp( List<Entry<String, Value>> childs, Object obj ) {
			Value v = new Value();
			v.childs = childs;
			v.obj = obj;
			return v;
		}

		public static Value makeArray( List<Value> childs, Object obj ) {
			Value v = new Value();
			v.isArray = true;
			v.arrChilds = childs;
			v.obj = obj;
			return v;
		}
	}



	public static Value getValues( Object o )
	{
        try {
			return getValuesUnchecked( o, Li.empty() );
		} catch ( IllegalArgumentException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		} catch ( IllegalAccessException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}
	}


	private static boolean writeValue( Value o, StringBuilder b )
	{
		boolean handled = true;

		if ( o.obj == null ) {
			b.append( o.obj );
		} else if (
				o.isPrimitive()
//				|| o.obj instanceof Collection<?>
			)
		{
			b.append( o.obj );
		} else if ( o.obj instanceof String ) {
			b.append( '"' ).append( o.obj ).append( '"' );
//		} else if ( o.obj.getClass().isArray() ) {
//			b.append( Arrays.asList( o ) );
		} else {
			handled = false;
		}

		return handled;
	}

	public static void writeArray( Iterable<Value> l, StringBuilder b )
	{
		boolean isFirst = true;

		b.append( "[" );

		for ( Value e : l ) {
			b.append( isFirst ? "" : ", " );
			isFirst = false;

			writeValues( e, b );
		}

		b.append( "]" );
	}

    public static void writeValues( Value o, StringBuilder b )
	{
    	if ( writeValue( o, b ) ) {
    		return;
    	} else if ( o.isArray() ) {
    		writeArray( o.arrChilds, b );
    	} else {
			boolean isFirst = true;

			b.append( "<" );

			for (Entry<String, Value> e : o.childs ) {
				b.append( isFirst ? "" : ", " )
					.append( e.name )
					.append( " = " );

				writeValues( e.value, b );

				isFirst = false;
			}

			b.append( ">" );
		}
	}

	public static String valuesToString( Value o ) {
		StringBuilder b = new StringBuilder();
		writeValues( o, b );
		return b.toString();
	}

	public static void main( String[] args )
	{
		Object d = Arrays.asList( 1, 2, 3 );

		System.out.println( "v = " + valuesToString( getValues( d ) ) );

//		List<Field> l = getAllFields( Double.class );
//		for ( Field f : l ) {
//			System.out.println( f.getName() );
//		}
//		System.out.println( "l = " + l );
	}

}


class HierarchyIterator implements Iterator<Class<?>>
{
	private Class<?> cls;

	public HierarchyIterator( Class<?> cls ) {
	    this.cls = cls;
    }

	@Override
    public boolean hasNext() {
	    return cls != null;
    }

	@Override
    public Class<?> next()
    {
		Class<?> c = cls;
		cls = cls.getSuperclass();
	    return c;
    }

	@Override
    public void remove() {
	    throw new UnsupportedOperationException("remove not supported");
    }
}

class Obj
{
	public String toDbgString() {
		return Util.deepToString( this );
	}
}

class Entry<K, V> {
	public K name;
	public V value;
	public Class<?> cls;

	private boolean isNested = false;

	public boolean isNested() {
		return isNested;
	}

	private Entry( K key, V value ) {
        this.name = key;
        this.value = value;
    }

	public static <K, V> Entry<K, V> make( K key, V value ) {
		return new Entry<K, V>( key, value );
	}
}




package j.util.util;

/**
 * An enum that can be used in various situation when errors, warnings and
 * info items are reported.
 *
 * Enable different components that handle issuse to cooperate even if they are
 * unrelated.
 */
public enum IssueType {
	ERROR,
	WARNING,
	INFO,
	OK
}

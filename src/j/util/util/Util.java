package j.util.util;



import j.util.iostreams.IoStreamUtil;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;

public class Util
{
	private Util() {}
	
	public static void throwRuntimized(Exception exc) {
	    if (exc instanceof RuntimeException) {
	        throw (RuntimeException) exc;
	    } else {
	        throw new RuntimeException(exc);
	    }
	}
	
	public static void clearStringBuilder( StringBuilder b ) {
		b.delete( 0, b.length() );
	}

	public static void replaceStringBuilder( StringBuilder b, int i ) {
		clearStringBuilder( b );
		b.append( i );
	}

	public static String exceptionToString( Throwable tbl )
	{
		StringWriter writer = new StringWriter();
		tbl.printStackTrace( new PrintWriter( writer ) );
		return writer.toString();

	}

	public static boolean twinEquals( Object a1, Object a2, Object b1, Object b2 ) {
		return equals( a1, a2 ) && equals( b1, b2 );
	}

	/**
	 * Use reflection to make a simple toString method.
	 *
	 * o is the object (often this), values are the values that should be
	 * included.
	 */
	public static String simpleToString( Object o, Object... values )
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "<" ).append( o.getClass().getSimpleName() );

		for ( Object v : values ) {
			builder.append( " " ).append( v );
		}

	    return builder.append( ">" ).toString();
	}

	public static boolean equals( Object o1, Object o2 ) {
		return o1 == o2 || ( o1 != null && o1.equals( o2 ) );
	}

	public static boolean equals( Object o1, Object o2, Object o3 ) {
		return equals( o1, o2 ) && equals( o2, o3 );
	}

	public static boolean equals( Object... args ) {

		for ( int i = 0; i < args.length - 1; i++ ) {

			if ( !equals( args[i], args[i + 1] ) ) {
				return false;
			}
		}

		return true;
	}

	public static <E> boolean isNullOrEmpty( Collection<E> c ) {
		return c == null ? true : c.isEmpty();
	}

	public static String deepToString( Object o ) {
		return ReflUtil.valuesToString( ReflUtil.getValues( o ) );
	}

	public static <T extends Comparable<? super T>> int compareNullabe(T o1, T o2)
	{
		if ( o1 == null && o2 == null) {
				return 0;
		} else if ( o1 == null && o2 != null ) {
			return 1;
		} else if ( o1 != null && o2 == null ) {
			return -1;
		} else if ( o1 != null && o2 != null ) {
			return o1.compareTo( o2 );
		} else {
		    throw new RuntimeException( "WTF?! Unreachably location." );
		}
	}


	public static void close( Closeable c )
	{
		if ( c == null ) return;

		try {
			c.close();
		} catch ( IOException e ) {
			// Ignore
		}
	}

	public static String nullToEmpty( String s )
	{
		return s == null ? "" : s ;
	}

	public static <T extends Comparable<T>> boolean inRange( final T d, final T l1, final T l2 )
	{
		return  l1.compareTo( d )  <= 0 && d.compareTo( l2 )  < 0;
	}

	public static int doubleHashCode( double value )
	{
		long bits = Double.doubleToLongBits(value);
		return (int)(bits ^ (bits >>> 32));
	}

	public static void throwFileNotFound( String msg, String file ) {
		throw new RuntimeException(
				new FileNotFoundException( msg + " can not be found: " + file ) );
	}

	public static double clamp( double value, double low, double high )
	{
		if ( value < low ) return low;
		if ( value > high ) return high;

		return value;
	}

	public static double clampHigh( double value, double limit )
	{
		return Math.min( value, limit );
	}

	public static <T extends Comparable<? super T>> int compare(T o1, T o2) {
		return o1.compareTo( o2 );
	}

	public static boolean inRange( final double d, final double low, final double high )
	{
		return low <= d && d <= high;
	}

	@SuppressWarnings("unchecked")
	public static <T> Collection<T> nullToEmpty( Collection<T> coll )
	{
		return coll == null ? Collections.EMPTY_LIST : coll;
	}

	public static void sleep( int ms ) {
		try {
			Thread.sleep( ms );
		} catch (InterruptedException e) { ; }
	}

	public static void dispose( Disposable d )
	{
		if ( d != null ) {
			d.dispose();
		}
	}

	// TODO: Maybe this should look at chars after . instead?
	public static FilenameFilter sufixFileFilter( final String sufix )
	{
		return new FilenameFilter() {
			public boolean accept( File dir, String name ) {
				return name.endsWith( sufix );
			}
		};
	}

	public static String expandHome( String file ) {
	    if ( file.startsWith( "~" ) ) {
            return System.getProperty( "user.home" ) + file.substring( 1 );
	    } else {
	        return file;
	    }
	}
	
    public static Iterable<String> readFile( File f ) throws FileNotFoundException  {
        return IoStreamUtil.lineIterable( new FileInputStream( f ) );
    }
    
    public static Iterable<String> readFile( String s ) throws FileNotFoundException  {
        return readFile( new File( s ) );
    }

}


package j.util.util;

import j.util.eventhandler.Parent;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

import com.google.common.base.Predicate;

public class AnnotUtil {

	private AnnotUtil() {}

	public static Predicate<AnnotatedElement>
		presentPred( Class<? extends Annotation> cls )
	{
		return
			new Predicate<AnnotatedElement>() {
				public boolean apply( AnnotatedElement input ) {
					return input.isAnnotationPresent( Parent.class );
				}
			};
	}
}

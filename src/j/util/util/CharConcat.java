package j.util.util;

import java.util.NoSuchElementException;

/**
 * A lazy concatination of CharSequences.
 */
public class CharConcat implements CharSequence {
	private CharSequence[] strings;
	private int length = 0,
		lastIndex = 0,
		lastString = 0,
		lastStringStart = 0;



	public CharConcat( CharSequence... strings )
	{
		this.strings = strings;

		for ( int i = 0; i < strings.length; i++ ) {
			if ( strings[ i ] == null ) strings[ i ] = EMPTY_SEQUENCE;
			length += strings[ i ].length();
		}
	}

	@Override public int length() {
		return length;
	}

	public void setSeq( int i1, CharSequence s1, int i2, CharSequence s2 )
	{
		if ( s1 == null ) s1 = EMPTY_SEQUENCE;
		if ( s2 == null ) s2 = EMPTY_SEQUENCE;

		length += s1.length() -  strings[ i1 ].length();
		strings[ i1 ] = s1;

		length += s2.length() -  strings[ i2 ].length();
		strings[ i2 ] = s2;

		reset();
	}

	public void setSeq( int i, CharSequence s )
	{
		length += s.length() -  strings[ i ].length();
		strings[ i ] = s;
		reset();
	}

	@Override
	public char charAt( int index )
	{
		if ( index < lastIndex ) {
			reset();
		} else {
			lastIndex = index;
		}

		index -= lastStringStart;

		for ( CharSequence s = strings[ lastString ];
			lastString < strings.length; lastString++ ) {

//			s = strings[ lastString ];
			int strLength = s.length();

			if ( index < strLength ) {
				return s.charAt( index );
			}

			lastStringStart += strLength;
			index -= strLength;
		}

		throw new IndexOutOfBoundsException();
	}

	private void reset() {
		lastIndex = 0;
		lastStringStart = 0;
		lastString = 0;
	}

	@Override public CharSequence subSequence( int start, int end ) {
		// TODO Auto-generated method stub
		return null;
	}


	public static final EmptySequence EMPTY_SEQUENCE = new EmptySequence();

	private static class EmptySequence implements CharSequence {

		@Override public int length() {
			return 0;
		}

		@Override public char charAt( int index ) {
			throw new NoSuchElementException();
		}

		@Override public CharSequence subSequence( int start, int end ) {
			throw new NoSuchElementException();
		}

	}
}

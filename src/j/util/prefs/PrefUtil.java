package j.util.prefs;

import j.util.tree.UniTreeNode;
import j.util.util.ReflUtil;
import j.util.util.StringUtil;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public final class PrefUtil
{
	private PrefUtil() {}

	private static Map<Class<?>, PrefHanler> prefHandlers = new HashMap<Class<?>, PrefHanler>();


	interface PrefHanler {
		public Object read( Preferences prefs, String key, Object def );
		public void write( Preferences prefs, String key, Object value );
		public Object fromString( String s );
	}

	public static Preferences load( Preferences prefs, Object o ) throws BackingStoreException
	{
		try {
			return doPrefAction( prefs, o, false, false );
		} catch ( IllegalArgumentException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		} catch ( IllegalAccessException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}
	}


	public static void loadTree( Preferences prefs, UniTreeNode<?> node ) throws BackingStoreException
	{
		Preferences newPrefs = load( prefs, node );

		for ( UniTreeNode<?> n : node.getChildren() ) {
			loadTree( newPrefs, n );
		}
	}

	public static void saveTree( Preferences prefs, UniTreeNode<?> node ) throws BackingStoreException
	{
		Preferences newPrefs = save( prefs, node );

		for ( UniTreeNode<?> n : node.getChildren() ) {
			saveTree( newPrefs, n );
		}
	}

	public static Preferences save( Preferences prefs, Object o ) throws BackingStoreException
	{
		try {
			return doPrefAction( prefs, o, true, false );
		} catch ( IllegalAccessException exc ) {
			exc.printStackTrace();
			throw new RuntimeException( exc );
		}
	}



	public static Preferences doPrefAction( Preferences prefs, Object obj, boolean save, boolean inline )
			throws IllegalArgumentException, IllegalAccessException, BackingStoreException
	{
		String name = getPrefName( obj );

		Preferences newPrefs = inline ? prefs : prefs.node( name );

		for ( Field f : ReflUtil.getAllFields( obj.getClass() ) ) {

			f.setAccessible( true );
			Object value = f.get( obj );

			// A field with value null is ignored on both loading and saving
			if ( value == null ) continue;

			PrefName nameAnn = f.getAnnotation( PrefName.class );
			String fieldName = nameAnn == null ? f.getName() : nameAnn.value();

			if ( f.isAnnotationPresent( PrefValue.class ) ) {

				PrefHanler prefHanler = prefHandlers.get( value.getClass() );

				if ( save ) {
					prefHanler.write( newPrefs, fieldName, value );
				} else {
					Object v = prefHanler.read( newPrefs, fieldName, value );
					f.set( obj, v );
				}

			} else if ( f.isAnnotationPresent( PrefNode.class ) ) {
			    PrefNode n = f.getAnnotation( PrefNode.class );

				// There must be an existing Object here, otherwise do nothing
				if ( value != null ) {
					doPrefAction( newPrefs, value, save, n.inline() );
				}

			} else if ( f.isAnnotationPresent( PrefValueCollection.class ) ) {

				PrefHanler strPrefHanler = prefHandlers.get( String.class );
				@SuppressWarnings( "unchecked" )
				Collection<Object> coll = (Collection<Object>) value;

				if ( save ) {
					strPrefHanler.write( newPrefs, fieldName, StringUtil.collToString( coll ) );
				} else {
					String collStr = (String)
							strPrefHanler.read( newPrefs, fieldName, "" );

					List<String> valueStrs = StringUtil.stringToColl( collStr );
					PrefHanler hand = prefHandlers.get( f.getAnnotation( PrefValueCollection.class ).value() );

					for ( String s : valueStrs ) {
						coll.add( hand.fromString( s ) );
					}
				}

			} else if ( f.isAnnotationPresent( PrefNodeCollection.class ) ) {
                PrefNodeCollection n = f.getAnnotation( PrefNodeCollection.class );

				Preferences collPrefs = n.inline() ?
				        newPrefs : newPrefs.node( fieldName );

				Iterable<?> coll = (Iterable<?>) value;

				for ( Object child : coll ) {
					doPrefAction( collPrefs, child, save, false );
				}

			}
		}

		return newPrefs;

	}

	private static String getPrefName( Object obj ) throws IllegalArgumentException, IllegalAccessException
	{

		for ( Field f : ReflUtil.getAllFields( obj.getClass() ) ) {
			if ( f.isAnnotationPresent( PrefNodeName.class ) ) {
				f.setAccessible( true );
				return f.get( obj ).toString();
			}
		}

		throw new IllegalArgumentException(  obj.getClass().getName()
				+ " does not have field with the "
				+ PrefNodeName.class.getSimpleName() + " annotation"  );
	}

	static {
		prefHandlers.put( Integer.class, new PrefHanler() {
			public Object read( Preferences prefs, String key, Object def ) {
				return prefs.getInt( key, (Integer) def );
			}
			public void write( Preferences prefs, String key, Object value ) {
				prefs.putInt( key, (Integer) value );
			}
			public Object fromString( String s ) {
				return Integer.parseInt( s );
			}
		} );

		prefHandlers.put( Integer.TYPE, prefHandlers.get( Integer.class ) );

		prefHandlers.put( Boolean.class, new PrefHanler() {
			public Object read( Preferences prefs, String key, Object def ) {
				return prefs.getBoolean( key, (Boolean) def );
			}
			public void write( Preferences prefs, String key, Object value ) {
				prefs.putBoolean( key, (Boolean) value );
			}
			public Object fromString( String s ) {
				return Boolean.parseBoolean( s );
			}
		} );

		prefHandlers.put( Boolean.TYPE, prefHandlers.get( Boolean.class ) );

		prefHandlers.put( String.class, new PrefHanler() {
			public Object read( Preferences prefs, String key, Object def ) {
				return prefs.get( key, (String) def );
			}
			public void write( Preferences prefs, String key, Object value ) {
				prefs.put( key, (String) value );
			}
			public Object fromString( String s ) {
				return s;
			}
		} );

		prefHandlers.put( File.class, new PrefHanler() {
			@Override public Object read( Preferences prefs, String key, Object def ) {
				return new File( prefs.get( key, ((File) def).getPath() ) );
			}
			@Override public void write( Preferences prefs, String key, Object value ) {
				prefs.put( key, ((File) value).getPath() );
			}
			public Object fromString( String s ) {
				return new File( s );
			}
		} );
	}


}

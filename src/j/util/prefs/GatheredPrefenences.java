package j.util.prefs;

import j.util.util.Asserts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Implementation of Preferences that saved all nodes in one singles backing
 * node. Keys are prefixed with the node name to avoid collisions.
 */
public class GatheredPrefenences extends AbstractPreferences
{
	private final Preferences impl;
	private final String space;
	private final String sep = ".";
	private final char sepCh = sep.charAt( 0 );

	public static Preferences getForNode( Preferences impl ) {
		return new GatheredPrefenences( impl );
	}

	private GatheredPrefenences( GatheredPrefenences parent, Preferences impl,
			String name, String parentSpace )
	{
		super( parent, name );
		verifyName( name );
		if ( impl instanceof GatheredPrefenences ) throw new IllegalArgumentException();
		this.impl = impl;
		this.space = fullKey( parentSpace, name );
	}

	public GatheredPrefenences( Preferences impl ) {
		super( null, "" );
		verifyName( "" );
		if ( impl instanceof GatheredPrefenences ) throw new IllegalArgumentException();
		this.impl = impl;
		this.space = "";
	}

	private String fullKey( String space, String key )
	{
		if ( space.isEmpty() ) {
			return key;
		} else {
			return space + sep + key;
		}
	}

	private String translateKey( String key ) {
		return fullKey( space, key );
	}

	private String keyToSpace( String key )
	{
		int i = key.lastIndexOf( sepCh  );
		return i != -1 ? key.substring( 0, i ) : "";
	}

	private boolean isInSpace( String key ) {
		return space.equals( keyToSpace( key ) );
	}


	private void verifyName( String name ) {
		Asserts.arg( !name.contains( sep ), "'" + sep + "' not allowed in name: " + name );
	}


	@Override
	protected void putSpi( String key, String value ) {
		verifyName( key );
		impl.put ( translateKey( key ), value );
	}

	@Override
	protected String getSpi( String key ) {
		return impl.get( translateKey( key ), null );
	}

	@Override
	protected void removeSpi( String key ) {
		impl.remove( translateKey( key ) );
	}

	@Override
	protected void removeNodeSpi() throws BackingStoreException
	{
		for ( String k : impl.keys() ) {
			if ( isInSpace( k ) ) impl.remove( k );
		}
	}

	@Override
	protected String[] keysSpi() throws BackingStoreException
	{
		List<String> result = new ArrayList<String>();

		for ( String k : impl.keys() ) {
			if ( isInSpace( k ) ) result.add( k );
		}

		return result.toArray( new String[ result.size() ] );
	}

	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException
	{
		Set<String> result = new HashSet<String>();

		for ( String k : impl.keys() ) {

			if ( k.startsWith( space ) ) {
			    String s = k.substring( space.length() );
			    int i = s.indexOf( sep );

			    if ( i != -1 ) {
			        result.add( s.substring( 0, i ) );
			    }
			}
		}

		return result.toArray( new String[ result.size() ] );
	}

	@Override
	protected AbstractPreferences childSpi( String name ) {
		return new GatheredPrefenences( this, impl, name, space );
	}

	@Override
	protected void syncSpi() throws BackingStoreException {
		impl.sync();
	}

	@Override
	protected void flushSpi() throws BackingStoreException {
		impl.flush();
	}

}

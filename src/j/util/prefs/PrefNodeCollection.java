package j.util.prefs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * When preferances are saved, fields with this annotation (must be iterable)
 * are looped through and its content are processed recursivly for Pref*
 * annotations.
 *
 * Notice that nothing is added or removed from the collection, the elements are
 * just processed.
 */
@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.FIELD )
public @interface PrefNodeCollection {
    public boolean inline() default false;
}

package j.util.notifying;

import j.util.caller.BasicCaller;
import j.util.caller.BasicCaller2;
import j.util.functional.Action1;
import j.util.functional.Action2;

public class Notifyer<T>
{
	private T value;
	public BasicCaller<T> caller = new BasicCaller<T>();
	public BasicCaller2<T, T> caller2 = new BasicCaller2<T, T>();


	public Notifyer( T value ) {
		super();
		this.value = value;
	}


	public T get() {
		return value;
	}

	public void set( T newValue )
	{
		if ( this.value != newValue ) {
			caller.call( newValue );
			caller2.call( value, newValue );
			this.value = newValue;
		}
	}

	public void clear() {
		caller.clear();
	}

	public void addListener( Action1<? super T> lis ) {
		caller.addListener( lis );
	}

	public boolean removeListener( Action1<? super T> lis ) {
		return caller.removeListener( lis );
	}


	public void addListener( Action2<T, T> lis ) {
		caller2.addListener( lis );
	}


	public boolean removeListener( Action2<T, T> lis ) {
		return caller2.removeListener( lis );
	}



}

package j.util.lists;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import j.util.functional.PredD;
import j.util.lists.Lists;
import j.util.lists.Maps;
import j.util.util.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class ListsTest {

	private Map<String, String> deps =
		Maps.make( new Object[][] {
				{ "root", null },
				{ "a", "root" },
				{ "b", "root" },
				{ "c", "a" },
		} );
	
	private PredD<String> PRED = new PredD<String>() {
		@Override public Boolean run( String arg1, String arg2 ) {
			return Util.equals( deps.get( arg1 ), arg2 );
		}
	};
	
	private List<String> depList = Arrays.asList( "c", "a", "root", "b" );

	@Test
	public void testSortPartially()
	{
		List<String> result = new LinkedList<String>();
		
		Lists.sortPartially( depList, PRED, result );
		
		assertTrue( result.indexOf( "root" )  > result.indexOf( null ) );
		assertTrue( result.indexOf( "a" )  > result.indexOf( "root" ) );
		assertTrue( result.indexOf( "b" )  > result.indexOf( "root" ) );
		assertTrue( result.indexOf( "c" )  > result.indexOf( "root" ) );
		assertTrue( result.indexOf( "c" )  > result.indexOf( "a" ) );
	}
	
	@Test
	public void testSortPartiallyEmpty()
	{
		List<String> result = new LinkedList<String>();
		Lists.sortPartially( Collections.<String>emptyList(), PRED, result );
	}
	
	// This tests do not pass, fix that sometime.
//	@Test
	public void testIters()
	{
		assertEquals( "Simple add step failed", 
				Arrays.asList( 1, 2, 3, 4, 5 ), 
				Lists.iterToList( Lists.seq( 1, 5 ) ) );
		
		assertEquals( "Simple add step failed", 
						Arrays.asList( 5, 4, 3, 2, 1 ), 
						Lists.iterToList( Lists.seq( 5, 1 ) ) );
		
		assertEquals( "Simple add step failed", 
				Arrays.asList( 1 ), 
				Lists.iterToList( Lists.seq( 1, 1 ) ) );
		
		assertEquals( "Simple add step failed", 
				Arrays.asList(), 
				Lists.iterToList( Lists.seq( 1, 1 ) ) );
		
		assertEquals( "Simple add step failed", 
				Lists.iterEquals( 
						Arrays.asList( 1, 6, 11, 16), 
						Lists.seq( 1, 20, 5 ) ) );
		
		assertEquals( "Simple add step failed", 
				Arrays.asList( 16, 11, 6, 1 ), 
				Lists.iterToList( Lists.seq( 20, 1 ) ) );
		
		assertEquals( "Simple add step failed", 
				Arrays.asList( -20, -15, -10, -5, 0 ), 
				Lists.iterToList( Lists.seq( -20, 1, 5 ) ) );
	}

}

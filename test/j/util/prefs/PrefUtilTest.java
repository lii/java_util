package j.util.prefs;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

public class PrefUtilTest {

	private Preferences root;




	class PrefObj {

		@PrefValue
		public String s = "orig s";

		@PrefNodeName
		public String name = "Name";

		public String noPref = "noPref";

		@PrefValue
		private int i = -1;


		public int getI() {
			return i;
		}
	}

	class PrefObjPar {

		@PrefNode
		public PrefObj child = new PrefObj();

		@PrefNode
		public PrefObj nullChild;

		@PrefNodeName
		public String s = "Parent";
	}

	class CollObj {

		@PrefNodeName
		protected String name = "CollObj";

		@PrefValueCollection( Integer.class )
		public List<Integer> is = new ArrayList<Integer>();

		@PrefValueCollection( String.class )
		public Set<String> ss = new TreeSet<String>();

		@PrefValueCollection( String.class )
		public Collection<String> n = null;

		@PrefValueCollection( File.class )
		public List<File> fs = Lists.newArrayList();


	}


	class NodeCollObj {
		@PrefNodeName
		protected String name = "CollObj";

		@PrefNodeCollection
		public List<Object> l = new ArrayList<Object>();
	}


	@Before
	public void createRoot() {
//		root = Preferences.userNodeForPackage( getClass() );
		root = new GatheredPrefenences( Preferences.userNodeForPackage( getClass() ) );
	}

	@After
	public void removeRoot() throws BackingStoreException {
//		root.removeNode();
		root.clear();
		root.flush();
	}




	@Test
	public void testSimpleLoad() throws BackingStoreException
	{

		Preferences p = root.node( "Name" );

		p.put( "s", "new s" );
		p.putInt( "i", 1 );
		p.put( "noPref", "notUsed" );

		PrefObj o = new PrefObj();

		PrefUtil.load( root, o );

		assertEquals( "new s", o.s );
		assertEquals( 1, o.i );
		assertEquals( "noPref", o.noPref );
	}

	@Test
	public void testLoadTree() {
	}

	@Test
	public void testSaveTree() {
	}

	@Test
	public void testChildSave() throws BackingStoreException
	{
		PrefObjPar o = new PrefObjPar();
		o.child = new PrefObj();

		//////
		PrefUtil.save( root, o );
		//////

		Preferences p = root.node( "Parent" );

		assertTrue( p.nodeExists( "Name" ) );

		Preferences childPrefs = p.node( "Name" );

		assertEquals( "orig s", childPrefs.get( "s", "no s" ) );
	}


	@Test
	public void testChildLoad() throws BackingStoreException
	{
		Preferences parentPrefs = root.node( "Parent" );
		Preferences childPrefs = parentPrefs.node( "Name" );

		childPrefs.put( "s", "new s" );
		childPrefs.putInt( "i", 1 );

		PrefObjPar o = new PrefObjPar();

		PrefUtil.load( root, o );

		assertEquals( "new s", o.child.s );
		assertEquals( 1, o.child.i );
	}

	@Test
	public void testSimpleSave() throws BackingStoreException
	{
		PrefObj o = new PrefObj();

		PrefUtil.save( root, o );

		Preferences p = root.node( "Name" );

		assertEquals( "orig s", p.get( "s", "no s" ) );
		assertEquals( -1, p.getInt( "i", 99 ) );
		assertNull( p.get( "noPref", null ) );
	}


	@Test
	public void testCollSave() throws BackingStoreException
	{

		CollObj orig = new CollObj();

		orig.is = Arrays.asList( 1, 2, 3 );
		orig.ss = new TreeSet<String>();
		orig.n = null;
		orig.fs = Lists.newArrayList( new File( "asdf/asf" ) );

		PrefUtil.save( root, orig );

		CollObj after = new CollObj();
		PrefUtil.load( root, after );

		assertEquals( orig.is, after.is );
		assertEquals( orig.ss, after.ss );
		assertEquals( orig.n, after.n );
		assertEquals( orig.fs, after.fs );
	}

	@Test
	public void testNodeColl() throws BackingStoreException
	{
		NodeCollObj collObj = new NodeCollObj();

		PrefObj prefObj = new PrefObj();
		prefObj.s = "new s";
		prefObj.i = 3;

		PrefObjPar parObj = new PrefObjPar();
		parObj.child.s = "new child s";

		collObj.l.addAll( Arrays.asList( prefObj, parObj ) );

		PrefUtil.save( root, collObj );

		NodeCollObj newCollObj = new NodeCollObj();

		PrefObj newObj = new PrefObj();
		PrefObjPar newPar = new PrefObjPar();

		newCollObj.l.addAll( Arrays.asList( newPar, newObj ) );

		PrefUtil.load( root, newCollObj );

		assertEquals( collObj.l.size(), newCollObj.l.size() );
		assertEquals( prefObj.s, newObj.s );
		assertEquals( prefObj.i, newObj.i );
		assertEquals( newPar.child.s, parObj.child.s );
	}


}







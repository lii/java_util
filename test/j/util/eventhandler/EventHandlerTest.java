package j.util.eventhandler;

import static org.junit.Assert.assertEquals;
import j.util.eventhandler.EventHandler;
import j.util.eventhandler.EventHandlerImpl;
import j.util.eventhandler.Receiver;
import j.util.eventhandler.GroupName;
import j.util.eventhandler.EventHandler.Cashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class EventHandlerTest
{
	public static class E1
	{
		public String data;

		public E1( String data ) {
	        this.data = data;
        }
	}
	
	public static class E2
	{
		public int data;

		public E2( int data ) {
	        this.data = data;
        }
	}
	
	public GroupName root = new GroupName( "root" ),
		g1 = new GroupName( "g1" ),
		g2 = new GroupName( "g2" ),
		g3 = new GroupName( "g3" ),
		g4 = new GroupName( "g4" );
	
    @Test
	public void testPost()
	{
		EventHandler handler = new EventHandlerImpl();
		
		handler.addGroup( root );
		handler.addGroup( g1, root );
		handler.addGroup( g2, g1 );
		handler.addGroup( g3, g1 );
		handler.addGroup( g4, root );
		
		handler.getSettings().setThrowOnNoReceiver( false );
		handler.getSettings().setCasheStrategy( Cashing.PREBUILD );
		
//		Object groups = g( root,
//							g( g1,
//									g( g2 ),
//									g( g3 ) ),
//							g( g4 ) );
		
		
//		handler.addGroups( groups );
		
		// entry nr 1 written to by receiver nr 1, entry 2 by receiver 2 etc
		final List<Object> init =  Arrays.<Object>asList( "no_data", -1, -1 ),
				result = new ArrayList<Object>( init );
		
		
		final E1 e1 = new E1( "data" );
		final E2 e2 = new E2( 7 );
		final E2 e3 = new E2( 8 );
		
		handler.addReceiver(
				g3,
				E1.class, 
				new Receiver<E1>() {
					@Override public void receive( E1 event ) {
						result.set( 0, event.data );
                    }});

		handler.addReceiver(
				g1,
				E2.class, 
				new Receiver<E2>() {
					@Override public void receive( E2 event ) {
						result.set( 1, event.data );
                    }});
		
		handler.addReceiver(
				g4,
				E2.class,
				new Receiver<E2>() {
					@Override public void receive( E2 event ) {
						result.set( 2, event.data );
                    }});
		
		Collections.copy( result, init );
		handler.post( g3, e1 );
		handler.post( g1, e2 );
		assertEquals( Arrays.<Object>asList( "data", 7, -1 ), result );

		Collections.copy( result, init );
		handler.post( g2, e1 );
		handler.post( g4, e1 );
		handler.post( g3, e3 );
		assertEquals( Arrays.<Object>asList( "no_data", 8, -1 ), result );

		Collections.copy( result, init );
		handler.post( root, e1 );
		handler.post( root, e2 );
		handler.post( root, e3 );
		assertEquals( Arrays.<Object>asList( "data", 8, 8 ), result );

		Collections.copy( result, init );
		handler.post( g4, e1 );
		assertEquals( init, result );

		Collections.copy( result, init );
		handler.post( g4, e3 );
		
		assertEquals( Arrays.<Object>asList( "no_data", -1, 8 ), result );
	}
    
}







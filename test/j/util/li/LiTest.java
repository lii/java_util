package j.util.li;

import static org.junit.Assert.*;
import j.util.functional.Fun1;
import j.util.functional.Fun2;
import j.util.functional.Pred;
import j.util.lists.Li;

import java.util.Objects;

import org.junit.Test;

public class LiTest {
    
    @Test
    public void equals() {
        
        assertEquals( Li.make(1, 2, 3), Li.make(1, 2, 3) );
        assertEquals( Li.make(), Li.make() );
        assertFalse( Objects.equals( Li.make(1, 2, 3), Li.make(1, 2 ) ) );
    }
    
    @Test
    public void size() {
        assertEquals( 0, Li.make().size() ); 
        assertEquals( 1, Li.make(1).size() ); 
        assertEquals( 2, Li.make(1, 2).size() ); 
        assertEquals( 3, Li.make(1, 2, 3).size() ); 
    }
    
    @Test
    public void reverseDest() {
        Li<Integer> l1 = Li.make(1, 2, 3);
        l1.reverseDest();
        assertEquals( Li.make(3, 2, 1), l1 );
    }

    @Test
    public void foldMapFilter()
    {
        Li<Integer> l1 = Li.make( 1, 2, 3, 4, 5, 6 );

        Li<Integer> l2 = l1.filter( new Pred<Integer>() {
            public Boolean run( Integer arg ) {
                return arg % 3 == 0;
            }
        } );
        
        assertEquals( "filter", Li.make( 3, 6 ), l2 );
        
        Li<String> l3 = l2.map( new Fun1<Integer, String>() {
            public String run( Integer arg ) {
                return Integer.toString( arg * arg );
            }
        } );

        assertEquals( "map", Li.make( "9", "36" ), l3 );

        String s = l1.fold( new Fun2<Integer, String, String>() {
            public String run( Integer arg1, String arg2 ) {
                return arg2 + " \"" + arg1 + "\"";
            }
        }, "Fold:" );

        assertEquals( "Fold strange", "Fold: \"1\" \"2\" \"3\" \"4\" \"5\" \"6\"", s );
    }
}



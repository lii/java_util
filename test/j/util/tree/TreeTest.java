package j.util.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import j.util.eventhandler.Parent;
import j.util.lists.Trees;
import j.util.tree.Tree;
import j.util.tree.TreeImpl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

public class TreeTest
{
	@Test
	public void testTree()
	{
		TreeImpl<String> t =
			new TreeImpl<String>( "a",
					new TreeImpl<String>( "b",
							new TreeImpl<String>( "c" ),
							new TreeImpl<String>( "d" ) ),
					new TreeImpl<String>( "e" ),
					new TreeImpl<String>( "f" ) );

		List<String> list1 = new LinkedList<String>();
		for ( String s : t ) {
			list1.add( s );
		}
		assertEquals( new TreeImpl<Object>(), new TreeImpl<Object>() );

		assertEquals( Arrays.asList( "a", "b", "c", "d", "e", "f" ),  list1 );

		assertEquals( 6, t.size() );
		assertEquals( 0, new TreeImpl<Object>().size() );

		assertEquals(
				new TreeImpl<String>( "b",
						new TreeImpl<String>( "c" ),
						new TreeImpl<String>( "d" ) ),
				t.findSubtree( "b" ) );

		assertEquals( new TreeImpl<String>( "f" ), t.findSubtree( "f" ) );

		assertEquals( Arrays.asList( "a", "b", "d" ),
				t.path( "d" ) );

		assertEquals( t, t.findSubtree( "a" ) );

		assertEquals( null, t.findSubtree( "q" ) );

		assertEquals( t, t.copy() );

		assertTrue( t.isSubtree( t ) );
		assertTrue(t.isSubtree(
						new TreeImpl<String>( "b",
								new TreeImpl<String>( "c" ),
								new TreeImpl<String>( "d" ) ) ) );

		assertFalse( t.isSubtree(
						new TreeImpl<String>( "b",
								new TreeImpl<String>( "c" ) ) ) );

		assertFalse( t.isSubtree(
						new TreeImpl<String>( "b",
								new TreeImpl<String>( "c" ),
								new TreeImpl<String>( "d",
									new	TreeImpl<String>( "y" ) ) ) ) );

		assertFalse( t.isSubtree(
						new TreeImpl<String>( "b",
								new TreeImpl<String>( "b" ),
								new TreeImpl<String>( "d" ) ) ) );

		assertEquals( Arrays.asList( "a", "b", "e", "f", "c", "d" ),
				Lists.newArrayList( t.breadthIterator() ) );
	}


	@Test
    public void testBuildFromClass()
    {
    	Tree<String> tree = Trees.buildFromClass( TestSpec.class );

    	assertEquals( tree,
    			new TreeImpl<String>( "a",
    					new TreeImpl<String>( "b",
    							new TreeImpl<String>( "c" ),
    							new TreeImpl<String>( "d" ) ),
    					new TreeImpl<String>( "e" ),
    					new TreeImpl<String>( "f" ) ) );

    	assertEquals( new TreeImpl<String>(), Trees.buildFromClass(
    			new  Object() {}.getClass() ) );

    	assertEquals( new TreeImpl<String>(), Trees.buildFromClass(
    			new  Object() {}.getClass() ) );
    }


    /*
     * This is good when debugging; break on exeptions doesnt work i JUnit.
     */
    public static void main( String[] args ) {
    	new TreeTest().testBuildFromClass();
    }

    private static class TestSpec {

	    @Parent("")
		public static String A = "a";

		@Parent("A")
		public static String B = "b", E = "e", F = "f";

		@Parent("B")
		public static String C = "c", d = "d";

    }
}

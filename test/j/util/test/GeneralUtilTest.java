package j.util.test;

import static org.junit.Assert.*;
import j.util.util.StringUtil;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class GeneralUtilTest {


	@Test
	public void testCollToString()
	{
		List<String> l1 = Arrays.asList( "woo", "wah", "hm" );
		String s1 = StringUtil.collToString( l1 );
		List<String> l2 = StringUtil.stringToColl( s1 );
		assertEquals( l1, l2 );

		List<String> l3 = Arrays.asList( "w|oo||||\\\\\\", "|wah\\|", "\\\\\\||hm" );
		String s2 = StringUtil.collToString( l3 );
		List<String> l4 = StringUtil.stringToColl( s2 );
		assertEquals( l3, l4 );
	}

	@Test
	public void testSplitWithQuotes()
	{
		assertEquals( Arrays.asList( "asdf" ), StringUtil.splitWithQuotes( "asdf" ) );
		assertEquals( Arrays.asList( "asdf" ), StringUtil.splitWithQuotes( "a\\sdf" ) );
		assertEquals( Arrays.asList( "as", "df" ), StringUtil.splitWithQuotes( "  as  df " ) );
		assertEquals( Arrays.asList( "as df" ), StringUtil.splitWithQuotes( "as\" \"df" ) );
		assertEquals( Arrays.asList( "as' 'df" ), StringUtil.splitWithQuotes( "as\"' '\"df" ) );
		assertEquals( Arrays.asList( "as\"d\\f" ), StringUtil.splitWithQuotes( "as\\\"d\\\\f" ) );
		assertEquals( Arrays.asList( "as df" ), StringUtil.splitWithQuotes( "as\\ df" ) );
		assertEquals( Arrays.asList( "as ", "df" ), StringUtil.splitWithQuotes( "as\" \" df" ) );
		assertEquals( Arrays.asList( "asdf" ), StringUtil.splitWithQuotes( "''asdf" ) );
		assertEquals( Arrays.asList( "as\"df" ), StringUtil.splitWithQuotes( "as'\"'df" ) );
		assertEquals( Arrays.asList( "", "asdf" ), StringUtil.splitWithQuotes( "'' asdf" ) );

		try {
			StringUtil.splitWithQuotes( "'" );
			assertTrue( "Should have thrown", false );
		} catch ( IllegalArgumentException exc ) {
		}

		try {
			StringUtil.splitWithQuotes( "a'\"'' \\'sdf" );
			assertTrue( "Should have thrown", false );
		} catch ( IllegalArgumentException exc ) {
		}

		try {
			StringUtil.splitWithQuotes( "asdf\\" );
			assertTrue( "Should have thrown", false );
		} catch ( IllegalArgumentException exc ) {
		}
	}

}
